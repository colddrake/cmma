var mongoose = require('mongoose');
var mongoosePaginate = require('mongoose-paginate');
var AutoIncrement = require('mongoose-sequence')(mongoose);


var CmmaResultSchema = new mongoose.Schema({
    
    cmpid:Number,
    username:String,
    project:String,
    domainname:String,
    appname:String,
    mapping: String,
    processarea: String,
    parameter:String,
    currentlevel:String,   
})

CmmaResultSchema.plugin(mongoosePaginate)
CmmaResultSchema.plugin(AutoIncrement,{inc_field:'cmmarid'})
const CmmaResult = mongoose.model('CmmaResult', CmmaResultSchema)

module.exports = CmmaResult;


    










