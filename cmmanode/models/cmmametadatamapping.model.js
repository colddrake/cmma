var mongoose = require('mongoose');
var mongoosePaginate = require('mongoose-paginate');
var AutoIncrement = require('mongoose-sequence')(mongoose);


var CmmaMetadataMappingSchema = new mongoose.Schema({
    mapping: String,
    processarea: String
})

CmmaMetadataMappingSchema.plugin(mongoosePaginate)
CmmaMetadataMappingSchema.plugin(AutoIncrement,{inc_field:'cmdmid'})
const CmmaMetadataMapping = mongoose.model('CmmaMetadataMapping', CmmaMetadataMappingSchema)

module.exports = CmmaMetadataMapping;










