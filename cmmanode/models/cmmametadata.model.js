var mongoose = require('mongoose');
var mongoosePaginate = require('mongoose-paginate');
var AutoIncrement = require('mongoose-sequence')(mongoose);


var CmmaMetadataSchema = new mongoose.Schema({
    cmdmid:Number,
    name: String,
    parameter: String,
    level1: String,
    level2: String,
    level3: String,
    level4: String,
    level5: String,
})

CmmaMetadataSchema.plugin(mongoosePaginate)
CmmaMetadataSchema.plugin(AutoIncrement,{inc_field:'cmdid'})
const CmmaMetadata = mongoose.model('CmmaMetadata', CmmaMetadataSchema)

module.exports = CmmaMetadata;










