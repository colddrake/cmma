var mongoose = require('mongoose');
var mongoosePaginate = require('mongoose-paginate');
var AutoIncrement = require('mongoose-sequence')(mongoose);


var ProjectSchema = new mongoose.Schema({
    name: String,
    description: String
})

ProjectSchema.plugin(mongoosePaginate)
ProjectSchema.plugin(AutoIncrement,{inc_field:'prjid'})
const Project = mongoose.model('Project', ProjectSchema)

module.exports = Project;










