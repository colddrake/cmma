var mongoose = require('mongoose');
var mongoosePaginate = require('mongoose-paginate');
var AutoIncrement = require('mongoose-sequence')(mongoose);



var DomainAppUserSchema = new mongoose.Schema({
    uid: Number,
    dmnid:Number,
    appid: Number       
})

DomainAppUserSchema.plugin(mongoosePaginate)
DomainAppUserSchema.plugin(AutoIncrement,{inc_field:'dmauid'})
const DomainAppUser = mongoose.model('DomainAppUser', DomainAppUserSchema)

module.exports = DomainAppUser;










