var mongoose = require('mongoose');
var crypto = require('crypto');
var jwt = require('jsonwebtoken');
var mongoosePaginate = require('mongoose-paginate');
var AutoIncrement = require('mongoose-sequence')(mongoose);
var passportLocalMongoose = require('passport-local-mongoose');



var UserActivitiesSchema = new mongoose.Schema({
    uid:Number,
    lastSuccessfullLogin:Date
})



UserActivitiesSchema.plugin(mongoosePaginate)
//UserSchema.plugin(passportLocalMongoose)
UserActivitiesSchema.plugin(AutoIncrement,{inc_field:'uaid'})
const UserActivities = mongoose.model('UserActivities', UserActivitiesSchema)

module.exports = UserActivities;










