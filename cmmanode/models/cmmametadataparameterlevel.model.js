var mongoose = require('mongoose');
var mongoosePaginate = require('mongoose-paginate');
var AutoIncrement = require('mongoose-sequence')(mongoose);


var CmmaMetadataParameterLevelSchema = new mongoose.Schema({
    parameter: String,
    level1: String,
    level2: String,
    level3: String,
    level4: String,
    level5: String,
    cmdmid: Number
})

CmmaMetadataParameterLevelSchema.plugin(mongoosePaginate)
CmmaMetadataParameterLevelSchema.plugin(AutoIncrement,{inc_field:'cmplid'})
const CmmaMetadataParameterLevel = mongoose.model('CmmaMetadataParameterLevel', CmmaMetadataParameterLevelSchema)

module.exports = CmmaMetadataParameterLevel;










