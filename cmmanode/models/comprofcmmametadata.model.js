var mongoose = require('mongoose');
var mongoosePaginate = require('mongoose-paginate');
var AutoIncrement = require('mongoose-sequence')(mongoose);


var CompanyProfileCmmaDataSchema = new mongoose.Schema({
    uid:Number,
    dmnid:Number,
    appid:Number,
    prjid:Number,
    mapping: String,
    processarea: String,
    parameter: String,
    level1: String,
    level2: String,
    level3: String,
    level4: String,
    level5: String,
})

CompanyProfileCmmaDataSchema.plugin(mongoosePaginate)
CompanyProfileCmmaDataSchema.plugin(AutoIncrement,{inc_field:'cpcmid'})
const CompanyProfileCmmaData = mongoose.model('CompanyProfileCmmaData', CompanyProfileCmmaDataSchema)

module.exports = CompanyProfileCmmaData;










