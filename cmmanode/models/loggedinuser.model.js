var mongoose = require('mongoose');
var crypto = require('crypto');
var jwt = require('jsonwebtoken');
var mongoosePaginate = require('mongoose-paginate');
var AutoIncrement = require('mongoose-sequence')(mongoose);
var passportLocalMongoose = require('passport-local-mongoose');



var LoggedInUserSchema = new mongoose.Schema({
    isUserVerified: Boolean,
    authToken:String
})



LoggedInUserSchema.plugin(mongoosePaginate)
const LoggedInUser = mongoose.model('LoggedInUser', LoggedInUserSchema)

module.exports = LoggedInUser;










