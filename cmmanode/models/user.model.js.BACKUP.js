var mongoose = require('mongoose');
var crypto = require('crypto');
var jwt = require('jsonwebtoken');
var mongoosePaginate = require('mongoose-paginate');
var AutoIncrement = require('mongoose-sequence')(mongoose);
var passportLocalMongoose = require('passport-local-mongoose');



var UserSchema = new mongoose.Schema({
    fullname: String,
    username: {
        type: String,
        required: true
    },
    password: String,
    email: {
        type:String,
        unique:true,
        required:true
    },
    contactnumber: String,
    role:String,
    createdbyid: Number,
    cmpid: Number,
    dmid: Number
    //hash:String,
    //salt:String

})



UserSchema.plugin(mongoosePaginate)
//UserSchema.plugin(passportLocalMongoose)
UserSchema.plugin(AutoIncrement,{inc_field:'uid'})
const User = mongoose.model('User', UserSchema)

module.exports = User;










