
var Project = require('../models/project.model')
var mongodb = require('mongodb');


_this = this

exports.createProject = async function(project){
    
    var newProject = new Project({
        name:project.name,
        description:project.description        
    })


    try{       
    
        var savedProject = await newProject.save();
        return savedProject;
    }catch(e){
        throw Error("Error occured while creating a project")
    }    
    
}

exports.getProjects = async function(query){       
    
    try {
        var projects = await Project.paginate(query)
        
        
        return projects;

    } catch (e) {

        
        throw Error('Error while Paginating projects')
    }
}












