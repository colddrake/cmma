
var DomainAppUser = require('../models/domainappuser.model')
var mongodb = require('mongodb');


_this = this



exports.getDomainApplicationUsers = async function(query){   
    
    
    try {
        var domainAppUsers = await DomainAppUser.paginate(query)
        
        
        return domainAppUsers;

    } catch (e) {

        
        throw Error('Error while Paginating Domain Application Users')
    }
}

exports.getDomainAppUsersByUid = async function(uid){   
        
    try {
        var domainAppUsersByUid = await DomainAppUser.find({uid:uid})
        
        
        return domainAppUsersByUid;

    } catch (e) {

        
        throw Error('Error while Paginating Domain Application Users By Uid')
    }
}

exports.getDomainAppUsersByDmid = async function(dmid){   
        
    try {
        var domainAppUsersByDmid = await DomainAppUser.find({dmid:dmid})
        
        
        return domainAppUsersByDmid;

    } catch (e) {

        
        throw Error('Error while Paginating Domain Application Users By Dmid')
    }
}

exports.getDomainAppUsersByAppid = async function(appid){   
        
    try {
        var domainAppUsersByAppid = await DomainAppUser.find({appid:appid})
        
        
        return domainAppUsersByAppid;

    } catch (e) {

        
        throw Error('Error while Paginating Domain Application Users By Appid')
    }
}

exports.createDomainApplicationUser = async function(applicationuser){
    
    var newDomainAppliactionUser = new DomainAppUser({
        uid:applicationuser.uid,
        appid:applicationuser.appid,
        dmnid:applicationuser.dmnid                 
    })


    try{           
        var savedDomainApplicationUser = await newDomainAppliactionUser.save();
        return savedDomainApplicationUser;
    }catch(e){
        throw Error("Error occured while creating domain application user")
    }    
    
}




