// Gettign the Newly created Mongoose Model we just created 
var User = require('../models/user.model')
var UserActivities = require('../models/useractivities.model')
var LoggedInUser = require('../models/loggedinuser.model')
var mongodb = require('mongodb');
var bcrypt = require('bcryptjs');
var jwt = require('jsonwebtoken');
const saltRounds = 10;

// Saving the context of this module inside the _the variable
_this = this


// exports.updateUserActivities = async function(userActivities){

//     var userActivities = new UserActivities({
//         uid:userActivities.uid,
//         lastSuccessfullLogin:userActivities.lastSuccessfullLogin
//     })

//     try{       
    
//         var savedUserActivities = await userActivities.save();
//         return savedUserActivities;

//     }catch(e){
//         throw Error("Error occured while creating user activities")
//     }

// }



exports.login = async function(inputUser){


    var loggedInUser = new LoggedInUser();
    var userEmail = inputUser.email;
    var date = new Date();

    var user = new User({
        // username:user.data1,
        // password:user.data2,
        // fullname:user.data3,
        email:inputUser.email,
        password:inputUser.password
        // contactnumber:user.data5,
        // role:user.data6,
        // createdbyid:user.data7,
        // cmpid:user.data8
    })           
    
    
    try {
        console.log("In login service "+inputUser.email+" "+user.password);
        console.log("In login service "+userEmail);

        var verifiedUser = await User.find({email:userEmail});              
        
        console.log("In login service "+verifiedUser[0].email);

         if(verifiedUser[0] !== undefined &&  bcrypt.compareSync(user.password,verifiedUser[0].password)){

             console.log("User Verification Successfull");
             
             //loggedInUser.authToken = this.generateJwt(verifiedUser[0]);
             
                loggedInUser.authToken = this.generateJwt(verifiedUser[0]);
                loggedInUser.isUserVerified = true;

                var userActivities = new UserActivities({
                    uid:verifiedUser[0].uid,
                    lastSuccessfullLogin:date
                })

                try{       
    
                    //var savedUserActivities = userActivities.save();
                    var updateduserActivity = UserActivities.updateOne(
                        {uid:userActivities.uid},
                        {$set:{lastSuccessfullLogin:userActivities.lastSuccessfullLogin}},
                        {upsert:true,safe:false},
                        function(err,data){
                            if(err){
                                console.log(err);
                            }else{
                                console.log("UPSERT SUCCESSFULL");
                            }
                        }
                    );
                    
                    //return savedUserActivities;
            
                }catch(e){
                    throw Error("Error occured while creating user activities")
                }
             

         }else{

             console.log("User verification is failed");
             loggedInUser.isUserVerified = false;

        }
        
        //  bcrypt.compare(user.password,verifiedUser.password,function(err,result){
        //      if(result == true){
        //          console.log("User Verification Successfull");
        //          loggedInUser.isVerifiedUser = true;
        //          loggedInUser.authToken = this.generateJwt(verifiedUser);
        //      }else{
        //          console.log("User verification is failed");
        //          loggedInUser.isVerifiedUser = false;
        //      }
        //  })

        console.log(loggedInUser.isUserVerified+"" +loggedInUser.authToken);
        console.log(loggedInUser);
        return loggedInUser;
        //return verifiedUser;

    } catch (e) {        
        throw Error('Error while Paginating Users'+e)
    }
}

exports.create = async function(user){
    
    var newUser = new User({
        username:user.data1,
        password:user.data2,
        fullname:user.data3,
        email:user.data4,
        contactnumber:user.data5,
        role:user.data6,
        createdbyid:user.data7,
        cmpid:user.data8,
        dmid:user.data9
    })


    try{       
    
        var savedUser = await newUser.save();
        return savedUser;
    }catch(e){
        throw Error("Error occured while creating user")
    }
    // If no old Todo Object exists return false
    
}

exports.register = async function(user){
    
    
    var encryptedPassword = bcrypt.hashSync(user.password,10);

    console.log(encryptedPassword);


    try{

        var newUser = new User({

        username : user.username,
        password : encryptedPassword,
        fullname : user.fullname,
        email : user.email,
        role : user.role,
        cmpid : user.cmpid,
        contactnumber : user.contactnumber,
        createdbyid : user.createdbyid,
        createdDate : user.createdDate,
        updatedDate : user.updatedDate

        });

        console.log(newUser.password);
        console.log(encryptedPassword);

        

        console.log(newUser.email+" "+newUser.username);        
    
        var registeredUser = await newUser.save();
        console.log("User registered successfull");        
        return registeredUser;        
        
    }catch(e){
        throw Error("Error occured while user registration"+e)
    }
    // If no old Todo Object exists return false
    
}

exports.encryptPassword = async function(user){
    let encryptPassword = undefined;
    bcrypt.hash(user.password,saltRounds,function(err,hash){
        encryptPassword = hash;
    })

    return encryptPassword;
}



exports.getUsers = async function(query, param){

    
    
    try {
        
        var users = await User.paginate(query)     
        
        return users;

    } catch (e) {

        // return a Error message describing the reason 
        throw Error('Error while Paginating Uses')
    }
}

exports.getUsersByRole = async function(role){

    
    
    
    // Try Catch the awaited promise to handle the error 
    
    try {
        var users = await User.find({role:role})
        
        // Return the todod list that was retured by the mongoose promise
        return users;

    } catch (e) {

        // return a Error message describing the reason 
        throw Error('Error while Paginating Uses')
    }
}

exports.getUsersByCompany = async function(companyId){


      // Try Catch the awaited promise to handle the error 
    
    try {
        var users = await User.find({cmpid:companyId})
        
        // Return the todod list that was retured by the mongoose promise
        return users;

    } catch (e) {

        // return a Error message describing the reason 
        throw Error('Error while Paginating Uses')
    }
}

exports.getUsersById = async function(id){    
  
  try {
      var user = await User.find({_id:id})           
      return user;

  } catch (e) {

      throw Error('Error while fetching a user by _id.')
  }
}

exports.generateJwt = function(verifiedUser){    

    return jwt.sign({
        _id: verifiedUser._id,
        // email: verifiedUser.email,
        // name: verifiedUser.name,        
        // fullname: verifiedUser.fullname,
        // role: verifiedUser.role,
        // cmpid: verifiedUser.cmpid,
        // uid:verifiedUser.uid
    },process.env.JWT_SECRET);

};


// UserSchema.methods.setPassword = function(password) {
//     this.salt = crypto.randomBytes(16).toString('hex');
//     this.hash = crypto.pbkdf2Sync(password, this.salt, 1000, 64,'sha512').toString('hex');
// };

// UserSchema.methods.validatePassword = function(password) {
//     var hash = crypto.pbkdf2Sync(password, this.salt, 1000, 64,'sha512').toString('hex');
//     return this.hash === hash;
// };


// UserSchema.methods.generateJwt = function(){

//     // var expiry = new Date();
//     // expiry.setDate(expiry.getMinutes()+3);

//     return jwt.sign({
//         _id: this._id,
//         email: this.email,
//         name: this.name,
//         //exp: parseInt(expiry.getTime() / 1000),
//         fullname: this.fullname,
//         role: this.role,
//         cmpid: this.cmpid,
//         uid:this.uid
//     },process.env.JWT_SECRET    );

// };


