
var Application = require('../models/application.model')
var CmmaMetadataMapping = require('../models/cmmametadatamapping.model')
var CmmaMetadataParameterLevel = require('../models/cmmametadataparameterlevel.model')
var CompanyProfileCmmaData = require('../models/comprofcmmametadata.model')
var CmmaResult = require('../models/cmmaresult.model')
var mongodb = require('mongodb');


_this = this

exports.createCmmaResult = async function(cmmaResult){

    var newCmmaResult = new CmmaResult({ 
        
        cmpid:cmmaResult.cmdmid,
        username:cmmaResult.username,
        project:cmmaResult.project,
        domainname:cmmaResult.domainname,
        appname:cmmaResult.appname,
        mapping: cmmaResult.mapping,
        processarea:cmmaResult.processarea,
        parameter:cmmaResult.parameter,
        currentlevel:cmmaResult.currentlevel   

    })    

    try{       
    
        var savedCmmaResult = await newCmmaResult.save();
        return savedCmmaResult;
    }catch(e){
        throw Error("Error occured while creating cmma result")
    }    


}

exports.createCompanyCmmaParameterLevel = async function(companyCmmaParameterLevel){

    var newCompanycmmaParameterLevel = new CompanyProfileCmmaData({ 
        
        uid:companyCmmaParameterLevel.uid,
        dmnid:companyCmmaParameterLevel.dmnid,
        appid:companyCmmaParameterLevel.appid,
        prjid:companyCmmaParameterLevel.prjid,
        mapping:companyCmmaParameterLevel.mapping,
        processarea:companyCmmaParameterLevel.processarea,
        parameter:companyCmmaParameterLevel.parameter,
        level1:companyCmmaParameterLevel.level1,
        level2:companyCmmaParameterLevel.level2,
        level3:companyCmmaParameterLevel.level3,
        level4:companyCmmaParameterLevel.level4,
        level5:companyCmmaParameterLevel.level5

    })    

    try{       
    
        var savedCompanyCmmaParameterLevel = await newCompanycmmaParameterLevel.save();
        return savedCompanyCmmaParameterLevel;
    }catch(e){
        throw Error("Error occured while creating an company cmma parameter level")
    }    


}

exports.createCmmaMetadataMapping = async function(cmmaMetadataMapping){
    
    var newCmmaMetadataMapping = new CmmaMetadataMapping({
        mapping:cmmaMetadataMapping.mapping,
        processarea:cmmaMetadataMapping.processarea        
    })


    try{       
    
        var savedCmmaMetadataMapping = await newCmmaMetadataMapping.save();
        return savedCmmaMetadataMapping;
    }catch(e){
        throw Error("Error occured while creating an savedCmmaMetadataMappingn")
    }    
    
}

exports.getCmmaResult = async function(query){
    
    


    try{       
    
        var cmmaResult = await CmmaResult.paginate(query);
        return cmmaResult;
    }catch(e){
        throw Error("Error occured while paginating cmmaResult")
    }    
    
}

exports.getCmmaResultByPamameters = async function(domain,application,project,mapping,processarea,parameter){
    
    


    try{       
    
        var cmmaResult = await CmmaResult.find({domainname:domain,appname:application,project:project,mapping:mapping,processarea:processarea,parameter:parameter});
        return cmmaResult;
    }catch(e){
        throw Error("Error occured while paginating cmmaResult")
    }    
    
}

exports.getCmmaMetadataMappings = async function(query){
    
    


    try{       
    
        var cmmaMetadataMappings = await CmmaMetadataMapping.paginate(query,{limit:100});
        return cmmaMetadataMappings;
    }catch(e){
        throw Error("Error occured while paginating cmmaMetadataMappings")
    }    
    
}

exports.getCmmaMetadataMappingsByMappings = async function(mapping){
    
    


    try{       
    
        var cmmaMetadataMappings = await CmmaMetadataMapping.find({mapping:mapping});
        return cmmaMetadataMappings;
    }catch(e){
        throw Error("Error occured while paginating cmmaMetadataMappings by mappings")
    }    
    
}

exports.createCmmaMetadataParameterLevel = async function(cmmaMetadataParameterLevel){
    
    var newCmmaMetadataParameterLevel = new CmmaMetadataParameterLevel({
        cmdmid:cmmaMetadataParameterLevel.cmdmid,
        parameter:cmmaMetadataParameterLevel.parameter,
        level1:cmmaMetadataParameterLevel.level1,
        level2:cmmaMetadataParameterLevel.level2,
        level3:cmmaMetadataParameterLevel.level3,
        level4:cmmaMetadataParameterLevel.level4,
        level5:cmmaMetadataParameterLevel.level5        
    })


    try{       
    
        var savedCmmaMetadataParameterLevel = await newCmmaMetadataParameterLevel.save();
        return savedCmmaMetadataParameterLevel;
    }catch(e){
        throw Error("Error occured while creating cmma metadata parameter level")
    }    
    
}


exports.getCmmaMetadataParameterLevel = async function(query){
    try{       
    
        var cmmaMetadataParameterLevel = await CmmaMetadataParameterLevel.paginate(query,{limit:100});
        return cmmaMetadataParameterLevel;
    }catch(e){
        throw Error("Error occured while paginating cmmaMetadataParameterLevel")
    }    
}

exports.getCmmaMetadataParameterLevelByCmdmid = async function(cmdmid){
    try{
        var cmmaMetadataParameterLevel = await CmmaMetadataParameterLevel.find({cmdmid:cmdmid});
        return cmmaMetadataParameterLevel;
    }catch(e){
        throw Error("Error occured while paginating cmmaMetadataParameterLevel by cmdmid")
    }    
}

exports.getCompanyCmmaParameterLevel = async function(query){
    try{
        var companyProfileCmmaData = await CompanyProfileCmmaData.paginate(query);
        return companyProfileCmmaData;
    }catch(e){
        throw Error("Error occured while paginating companyProfileCmmaData")
    }
}

exports.getCompanyCmmaParameterLevelByMPAP = async function(mapping,processarea,parameter){
    try{
        var companyProfileCmmaData = await CompanyProfileCmmaData.find({mapping:mapping,processarea:processarea,parameter:parameter});
        return companyProfileCmmaData;
    }catch(e){
        throw Error("Error occured while paginating companyProfileCmmaData")
    }
}

