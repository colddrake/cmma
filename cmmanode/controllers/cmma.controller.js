var passport = require('passport');
var mongoose = require('mongoose');
var User = require('../models/user.model');
var CmmaMetadata = require('../models/cmmametadata.model');
var CmmaMetadataMapping = require('../models/cmmametadatamapping.model');

var LoginService = require('../services/user.service')
var CmmaService = require('../services/cmma.service')


var sendJSONResponse = function(res,status,content){
    res.status(status);
    res.json(content);
}



exports.createCmmaMetadataMapping = async function(req, res, next){

    // Id is necessary for the update        

    console.log(req.body)

    var cmmaMetadataMapping = { 
        mapping:req.body.mapping,
        processarea:req.body.processarea                     
    }

    try{
        var createdCmmaMetadataMapping = await CmmaService.createCmmaMetadataMapping(cmmaMetadataMapping)
        return res.status(200).json({status: 200, data: createdCmmaMetadataMapping, message: "Mapping Created Succesfully"})
    }catch(e){
        return res.status(400).json({status: 400., message: e.message})
    }
}

exports.getCmmaMetadataMappings = async function(req, res, next){

    try{
        
        var mappingType = req.query.mapping;

        if(mappingType !== undefined){
            var cmmaMappings = await CmmaService.getCmmaMetadataMappingsByMappings(mappingType)
        }else{
            var cmmaMappings = await CmmaService.getCmmaMetadataMappings({})                        
        }

        
        return res.status(200).json({status: 200, data: cmmaMappings, message: "CmmaMappings Received Succesfully"});
        
    }catch(e){               
        return res.status(400).json({status: 400, message: e.message});        
    }

 
}

exports.getCmmaMetadataMappingsByMappings = async function(req, res, next){

    try{   
        
        var cmmaMappingsByMapping = await CmmaService.getCmmaMetadataMappingsByMappings(mappingType);                        
        return res.status(200).json({status: 200, data: cmmaMappingsByMapping, message: "CmmaMappings By Mappings Received Succesfully"});
        
    }catch(e){               
        return res.status(400).json({status: 400, message: e.message});        
    }

 
}

exports.createCmmaMetadataParameterLevel = async function(req, res, next){    

    console.log(req.body)

    var cmmaMetadataParameterLevel = { 
        cmdmid:req.body.cmdmid,
        parameter:req.body.parameter,
        level1:req.body.level1,
        level2:req.body.level2,
        level3:req.body.level3,
        level4:req.body.level4,
        level5:req.body.level5

    }

    try{
        var createdCmmaMetadataParameterLevel = await CmmaService.createCmmaMetadataParameterLevel(cmmaMetadataParameterLevel)
        return res.status(200).json({status: 200, data: createdCmmaMetadataParameterLevel, message: "Parameter levels created succesfully"})
    }catch(e){
        return res.status(400).json({status: 400., message: e.message})
    }
}

 exports.getCmmaMetadataParameterLevel = async function(req, res, next){

    try{   
        var cmdmid = req.query.cmdmid;

        if(cmdmid !== undefined ){
            var cmmaMetadataPamameterLevel = await CmmaService.getCmmaMetadataParameterLevelByCmdmid(cmdmid);
        }else{
            var cmmaMetadataPamameterLevel = await CmmaService.getCmmaMetadataParameterLevel({});
        }

                                
        return res.status(200).json({status: 200, data: cmmaMetadataPamameterLevel, message: "Parameter levels received succesfully"});
        
    }catch(e){               
        return res.status(400).json({status: 400, message: e.message});        
    }

}

exports.createCompanyCmmaParameterLevel = async function(req, res, next){    

    console.log(req.body)

    var CompanycmmaParameterLevel = { 
        uid:req.body.uid,
        dmnid:req.body.dmnid,
        appid:req.body.appid,
        prjid:req.body.prjid,
        mapping:req.body.mapping,
        processarea:req.body.processarea,
        parameter:req.body.parameter,
        level1:req.body.level1,
        level2:req.body.level2,
        level3:req.body.level3,
        level4:req.body.level4,
        level5:req.body.level5

    }

    try{
        var createdCompanyCmmaParameterLevel = await CmmaService.createCompanyCmmaParameterLevel(CompanycmmaParameterLevel)
        return res.status(200).json({status: 200, data: createdCompanyCmmaParameterLevel, message: "Company Cmma paarameter levels created succesfully"})
    }catch(e){
        return res.status(400).json({status: 400., message: e.message})
    }
} 



 exports.getCompanyCmmaParameterLevel = async function(req, res, next){

    var mapping = req.query.mapping;
    var processarea = req.query.processarea;
    var parameter = req.query.parameter;   
    

     try{   

        if(mapping !== undefined && processarea !== undefined && parameter !== undefined){
            var companyCmmaPamameterLevel = await CmmaService.getCompanyCmmaParameterLevelByMPAP(mapping,processarea,parameter);
        }else{
            var companyCmmaPamameterLevel = await CmmaService.getCompanyCmmaParameterLevel({});
        }

         
         console.log("In node cmma controller "+companyCmmaPamameterLevel);   
         return res.status(200).json({status: 200, data: companyCmmaPamameterLevel, message: "Company cmma parameter levels received succesfully"});
        
     }catch(e){               
         return res.status(400).json({status: 400, message: e.message});        
     }

}

exports.createCmmaResult = async function(req, res, next){    

    console.log(req.body)

    var cmmaResult = { 
        cmpid:req.body.cmdmid,
        username:req.body.username,
        project:req.body.project,
        domainname:req.body.domainname,
        appname:req.body.appname,
        mapping: req.body.mapping,
        processarea:req.body.processarea,
        parameter:req.body.parameter,
        currentlevel:req.body.currentlevel
    }

    try{
        var cmmaResult = await CmmaService.createCmmaResult(cmmaResult)
        return res.status(200).json({status: 200, data: cmmaResult, message: "Cmma Results created succesfully"})
    }catch(e){
        return res.status(400).json({status: 400., message: e.message})
    }
}

exports.getCmmaResult = async function(req, res, next){    

    try{

        if(req.query.domainname !== undefined && req.query.appname !== undefined && req.query.project !== undefined && 
            req.query.mapping !== undefined && req.query.processarea !== undefined && req.query.parameter !== undefined){
                var cmmaResult = await CmmaService.getCmmaResultByPamameters(req.query.domainname,req.query.appname,req.query.project, 
                    req.query.mapping,req.query.processarea,req.query.parameter)   
        }else{
            var cmmaResult = await CmmaService.getCmmaResult({})    
        }

        
        return res.status(200).json({status: 200, data: cmmaResult, message: "Cmma Results received succesfully"})
    }catch(e){
        return res.status(400).json({status: 400., message: e.message})
    }
}
