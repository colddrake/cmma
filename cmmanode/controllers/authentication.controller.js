var passport = require('passport');
var mongoose = require('mongoose');
var User = require('../models/user.model');
var LoginService = require('../services/user.service')


var sendJSONResponse = function(res,status,content){
    res.status(status);
    res.json(content);
}

exports.register = async function(req,res) {
    
    if(!req.body.fullname || !req.body.email || !req.body.password){
        sendJSONResponse(res,400,{
            "message": "All fields required"
        });
        return;
    }

    
    var user = {
        fullname:     req.body.fullname,
        email:        req.body.email,        
        password:     req.body.password,
        username:     req.body.username,
        role:         req.body.role,    
        cmpid:        req.body.cmpid,
        contactnumber:req.body.contactnumber,    
        createdbyid:  req.body.createdbyid,
        createdDate:  req.body.createdDate,
        updatedDate:  req.body.updatedDate
    };   
    
    //user.setPassword(req.body.password);

    try{
        
        
    
        var registeredUser =await LoginService.register(user)
        //token = registeredUser.generateJwt();


        return res.status(201).json({status: 201, data: registeredUser, message: "User Registered Succesfully"})
    
    }catch(e){       
        
        return res.status(400).json({status: 400, message: "User Registation was unuccesfully"+e})
    }



}   
