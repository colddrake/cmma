// Accessing the Service that we just created

var DomainAppUserService = require('../services/domainappuser.service')

// Saving the context of this module inside the _the variable

_this = this



exports.createDomainApplicationUser = async function(req, res, next){

    // Id is necessary for the update        

    console.log(req.body)

    var applicationUser = {
        uid: req.body.uid,
        appid: req.body.appid,
        dmnid:req.body.dmnid
    }

    try{
        var createdDomainApplicationUser = await DomainAppUserService.createDomainApplicationUser(applicationUser)
        return res.status(200).json({status: 200, data: createdDomainApplicationUser, message: "Domain Application User Created Successfully"})
    }catch(e){
        return res.status(400).json({status: 400., message: e.messaDomaine})
    }
}

exports.getDomainApplicationUsers = async function(req, res, next){    

    try{
    
        var domainApplicationUsers = await DomainAppUserService.getDomainApplicationUsers({})                
        
        return res.status(200).json({status: 200, data: domainApplicationUsers, message: "Domain Application Users Recieved Succesfully"});
        
    }catch(e){               
        return res.status(400).json({status: 400, message: e.message});        
    }
}

exports.getDomainAppUsersByUid = async function(req, res, next){

    try{    
        var domainApplicationUsersByUid = await DomainAppUserService.getDomainAppUsersByUid(req.params.uid)                
        
        return res.status(200).json({status: 200, data: domainApplicationUsersByUid, message: "Domain Application Users by Uid Recieved Successfully"});
        
    }catch(e){       
        
        return res.status(400).json({status: 400, message: e.message});
        
    }
}

exports.getDomainAppUsersByDmid = async function(req, res, next){

    try{    
        var domainApplicationUsersByDmid = await DomainAppUserService.getDomainAppUsersByDmid(req.params.dmid)                
        
        return res.status(200).json({status: 200, data: domainApplicationUsersByDmid, message: "Domain Applications Users by Dmid Recieved Successfully"});
        
    }catch(e){       
        
        return res.status(400).json({status: 400, message: e.message});
        
    }
}

exports.getDomainAppUsersByAppid = async function(req, res, next){

    try{    
        var domainApplicationUsersByAppid = await DomainAppUserService.getDomainAppUsersByAppid(req.params.appid)                
        
        return res.status(200).json({status: 200, data: domainApplicationUsersByAppid, message: "Domain Applications by Appid Recieved Successfully"});
        
    }catch(e){       
        
        return res.status(400).json({status: 400, message: e.message});
        
    }
}






