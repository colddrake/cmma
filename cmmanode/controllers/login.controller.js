var passport = require('passport');
var mongoose = require('mongoose');
var User = require('../models/user.model');
var LoginService = require('../services/user.service');



var sendJSONResponse = function(res,status,content){
    res.status(status);
    res.json(content);
}

exports.login = async function(req,res) {
    
    if(!req.body.email || !req.body.password){
        sendJSONResponse(res,400,{
            "message": "All fields required"
        });
        return;
    }
    
    // var date = new Date();

    var user = {        
        email:        req.body.email,        
        password:     req.body.password        
    };
    
    
    
    //user.setPassword(req.body.password);

    try{      
        
        console.log("in login controller "+user.email+" "+user.password);
        var loggedInUser =await LoginService.login(user);        
        console.log("in login controller "+loggedInUser);

        if(loggedInUser.isUserVerified){            

            // var userActivities = {        
            //     uid = loggedInUser.uid,
            //     lastSuccessfullLogin = date
            // };

            // var savedUserActivities = await LoginService.updateUserActivities(userActivities);
            // console.log("user activities saved "+savedUserActivities);


            return res.status(201).json({status: 201, data: loggedInUser, message: "User Logged In Succesfully"})
         }else{
             return res.status(400).json({status: 400, message: "User Log In failed. Reason: user verification failed"})
         }

        
    
    }catch(e){       
        
        return res.status(400).json({status: 400, message: "User Login was unuccesfull"+e})
    }

    // passport.authenticate('local',  function(err,user,info){

    //     var token;

    //     if(err){
    //         sendJSONResponse(res,404,err);
    //         return;
    //     }

    //     if(user){

    //         token = user.generateJwt();    
    //         sendJSONResponse(res,200,{
    //             "token":token
    //         });
    //     }else{
    //         sendJSONResponse(res,200,info);
    //     }
    // }) (req, res);

}   
