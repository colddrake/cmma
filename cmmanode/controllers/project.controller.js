// Accessing the Service that we just created

var ProjectService = require('../services/project.service')

// Saving the context of this module inside the _the variable

_this = this

exports.createProject = async function(req, res, next){

    var project = {
        name: req.body.name,
        description: req.body.description        
    }

    try{
        var createdProject = await ProjectService.createProject(project)
        return res.status(200).json({status: 200, data: createdProject, message: "Project Created Successfully"})
    }catch(e){
        return res.status(400).json({status: 400., message: e.messaDomaine})
    }
}

exports.getProjects = async function(req, res, next){    

    try{    
        var projects = await ProjectService.getProjects({})                        
        return res.status(200).json({status: 200, data: projects, message: "Projects Recieved Successfully"});
        
    }catch(e){               
        return res.status(400).json({status: 400, message: e.message});        
    }
}












