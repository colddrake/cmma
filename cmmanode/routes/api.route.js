var express = require('express');
var jwt =require('express-jwt');
var router = express.Router()

var user = require('./api/users.route')
var company = require('./api/companies.route')
var dmappuser = require('./api/dmappusers.route')
var project = require('./api/projects.route')
var cmmamapping = require('./api/cmaamappings.route')
var cmmaparameterlevel = require('./api/cmmaparamlevel.route')
var cpcmmadata = require('./api/cpcmmadata.route')
var cmmaresult = require('./api/cmmaresult.route')

var AuthenticationController = require('../controllers/authentication.controller')
var LoginController = require('../controllers/login.controller')

router.use('/user',user);
router.use('/company',company);
router.use('/dmappuser',dmappuser);
router.use('/project',project);
router.use('/cmmamapping',cmmamapping);
router.use('/cmmaparameterlevel',cmmaparameterlevel);
router.use('/cpcmmadata',cpcmmadata);
router.use('/cmmaresult',cmmaresult);

router.post('/login',LoginController.login);
router.post('/register',AuthenticationController.register);


module.exports = router;




