var express = require('express')

var router = express.Router()
var appuser = require('./api/appusers.route')

router.use('/appuser',appuser);


module.exports = router;