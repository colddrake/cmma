var express = require('express')

var router = express.Router()

// Getting the Todo Controller that we just created

var DomainAppUserController = require('../../controllers/domainappuser.controller');


// Map each API to the Controller FUnctions


router.get('/', DomainAppUserController.getDomainApplicationUsers)
router.get('/:uid', DomainAppUserController.getDomainAppUsersByUid)
router.get('/:dmid', DomainAppUserController.getDomainAppUsersByDmid)
router.get('/:appid', DomainAppUserController.getDomainAppUsersByAppid)
router.post('/create', DomainAppUserController.createDomainApplicationUser)


// Export the Router

module.exports = router;