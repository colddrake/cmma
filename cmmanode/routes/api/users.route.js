var express = require('express')
var jwt =require('express-jwt');
var router = express.Router()

// Getting the Todo Controller that we just created

var LoginController = require('../../controllers/user.controller');

var auth = jwt({
    secret: process.env.JWT_SECRET,
    userProperty: 'payload'
});
// Map each API to the Controller FUnctions


router.get('/',LoginController.getUsers)
//router.get('/get/:role', LoginController.getUsersByRole)
router.post('/',auth, LoginController.createUser)
router.post('/login',auth, LoginController.userLogin)


// Export the Router

module.exports = router;