var express = require('express')

var router = express.Router()


var CmmaController = require('../../controllers/cmma.controller');


router.get('/',CmmaController.getCmmaMetadataParameterLevel)
router.post('/', CmmaController.createCmmaMetadataParameterLevel)

module.exports = router;