var express = require('express')

var router = express.Router()

// Getting the Todo Controller that we just created

var CompanyController = require('../../controllers/company.controller');

var jwt = require('express-jwt');
var auth = jwt({
  secret: process.env.JWT_SECRET,
  userProperty: 'payload'
});


// Map each API to the Controller FUnctions



router.post('/create', CompanyController.createCompany)
router.get('/',CompanyController.getCompanies)
router.get('/:cmpid', CompanyController.getCompaniesByCmpid)



// Export the Router

module.exports = router;