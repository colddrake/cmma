var express = require('express')

var router = express.Router()

var ProjectController = require('../../controllers/project.controller');


router.get('/', ProjectController.getProjects)
router.post('/', ProjectController.createProject)

// Export the Router

module.exports = router;