var express = require('express')

var router = express.Router()


var CmmaController = require('../../controllers/cmma.controller');



router.get('/',CmmaController.getCmmaResult)
router.post('/', CmmaController.createCmmaResult)

module.exports = router;