var express = require('express')

var router = express.Router()


var CmmaController = require('../../controllers/cmma.controller');



router.get('/',CmmaController.getCompanyCmmaParameterLevel)
router.post('/', CmmaController.createCompanyCmmaParameterLevel)

module.exports = router;