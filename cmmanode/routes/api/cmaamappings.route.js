var express = require('express')

var router = express.Router()


var CmmaController = require('../../controllers/cmma.controller');


router.get('/',CmmaController.getCmmaMetadataMappings)
router.get('/',CmmaController.getCmmaMetadataMappingsByMappings)
router.post('/', CmmaController.createCmmaMetadataMapping)

module.exports = router;