import {Injectable} from '@angular/core';
import {
    HttpEvent,HttpInterceptor,HttpHandler,HttpRequest
} from '@angular/common/http';
import {Observable} from 'rxjs';
import {CookieService} from 'ngx-cookie-service';

@Injectable()
export class AuthenticationInterceptor implements HttpInterceptor{

    constructor(
        private cookieService:CookieService    
      ) { }

    intercept(req:HttpRequest<any>,next:HttpHandler):
    Observable<HttpEvent<any>>{

        
            console.log('In auth interceptor '+req.url);        

            const token = this.cookieService.get('token');

            const authReq = req.clone({
                headers: req.headers.set('Authorization',token)
            });
            
            return next.handle(authReq);

        }
        
    }
