class Application {
    name: string;
    domain: String;    
    cmpid:Number;
    createdByUid:Number;
    appid:Number;

    constructor(
    ) {
        this.name = '';
        this.domain = '';        
        this.cmpid = 0;
        this.createdByUid = 0;
        this.appid = 0;
     }

}

export default Application;
