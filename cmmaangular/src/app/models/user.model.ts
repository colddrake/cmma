class User {
    username: String;
    password: String;
    fullname: String;
    email: String;
    contactnumber: String;
    role: String;
    createdbyid: Number;
    cmpid:Number;
    uid:Number;
    createdDate:Date;
    updatedDate:Date;    

    constructor(
    ) {
        this.username = '';
        this.password = '';
        this.fullname = '';
        this.email    = '';
        this.contactnumber = '';
        this.role = '';
        this.createdbyid = 0;
        this.cmpid = 0;        
        this.uid = 0;
        this.createdDate = new Date;
        this.updatedDate = new Date;
     }

}

export default User;
