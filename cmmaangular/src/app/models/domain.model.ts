class Domain {
    name: string;
    description: String;    
    cmpid:Number;
    dmid:Number;

    constructor(
    ) {
        this.name = '';
        this.description = '';        
        this.cmpid = 0;
        this.dmid = 0;
     }

}

export default Domain;
