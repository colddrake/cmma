class CmmaMetadataParameterLevel {
    parameter: string;
    level1: string;
    level2: string;
    level3: string;
    level4: string;
    level5: string;    
    cmdmid: Number;

    constructor(
    ) {
        this.parameter = '';
        this.level1 = '';
        this.level2 = '';
        this.level3 = '';
        this.level4 = '';
        this.level5 = '';        
        this.cmdmid = 0;              
     }

}

export default CmmaMetadataParameterLevel;
