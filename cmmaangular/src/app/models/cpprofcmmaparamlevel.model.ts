class CompanyProfileCmmaParameterLevel {
    cpcplid:Number;
    uid:Number;
    dmnid:Number;
    appid:Number;
    prjid:Number;
    mapping: String;
    processarea: String;
    parameter: String;
    level1: String;
    level2: String;
    level3: String;
    level4: String;
    level5: String;

    constructor(
    ) {

    this.cpcplid = 0;
    this.uid = 0;
    this.dmnid = 0;
    this.appid = 0;
    this.prjid = 0;
    this.mapping = '';
    this.processarea = '';
    this.parameter = '';
    this.level1 = '';
    this.level2 = '' ;
    this.level3 = '';
    this.level4 = '';
    this.level5 = '';

     }

}

export default CompanyProfileCmmaParameterLevel;
