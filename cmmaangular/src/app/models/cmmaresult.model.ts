class CmmaResult {
    cmmarid:Number;
    cmpid:Number;
    username:String;
    project:String;
    domainname:String;
    appname:String;
    mapping: String;
    processarea: String;
    parameter:String;
    currentlevel:String;    
    

    constructor(
    ) {
        this.cmmarid = 0;
        this.cmpid = 0;
        this.username = '';
        this.domainname = '';
        this.project = '';
        this.appname = '';
        this.mapping = '';
        this.processarea = '';
        this.parameter = '';
        this.currentlevel = '';
     }

}

export default CmmaResult;
