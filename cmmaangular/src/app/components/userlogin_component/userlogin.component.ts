import { Response } from '@angular/http';
import { UserService } from '../../services/user.service';
import User from '../../models/user.model';
import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import {AuthenticationService} from '../../services/authentication.service'
import { assertNotNull } from '@angular/compiler/src/output/output_ast';
import { EmailValidator } from '@angular/forms';
import {CookieService} from 'ngx-cookie-service';

@Component({
  // selector: 'app-user-login',
  templateUrl: './login.component.html',
  styleUrls: ['../../../assets/css/login.css']
})
export class UserLoginComponent implements OnInit {

  constructor(
    // Private todoservice will be injected into the component by Angular Dependency Injector
    private userService: UserService,
    private authenticationService: AuthenticationService,
    private cookieService:CookieService,
    private router: Router

  ) { }

  public newUser: User = new User();
  public loggedInUser: User = new User();  

  ngOnInit(): void {}

  login(){
    console.log('Inside Login')

    
        
    


    this.userService.login(this.newUser)
    .subscribe(status => {
      if(status){

    console.log('Login successfull '+status);
    this.userService.userValidationAndRedirections()
    .subscribe(res => {
     console.log(res);    
     this.authenticationService.setCurrentUser(res);
     this.router.navigate([res.role]);
    })

      }else{
        console.log('Login failed'+status);
      }

    
    


    // this.userService.getUsersById(this.cookieService.get('token'))
    // .subscribe(loggedInUser =>{
    
    //   this.router.navigate([loggedInUser.role]);
    // });
    // console.log(token);
    // this.authenticationService.currentUser();
    // console.log(this.authenticationService.currentUser());
    // console.log('Current user fetched')
    // this.authenticationService.isLoggedIn();
    // console.log('User logged in')
    // console.log(this.authenticationService.currentUser().role);



    // if(this.authenticationService.isLoggedIn()){
      
    // }

    });
    

  }

  // userValidateAndRedirect(): void{
  //   let returnedUser = undefined;    
  //   this.userService.userValidationAndRedirections()
  //   .subscribe(res => {
  //       this.loggedInUser = res;      
  //    })    
  //   // this.loggedInUser = returnedUser;
  //   //  console.log("In validate user "+returnedUser);
  //   //  console.log("In validate user "+returnedUser.role);
  //   //  console.log("In validate user "+this.loggedInUser.role);
  //   //  console.log("In validate user "+this.loggedInUser["data"].role);
  //   //  console.log("In validate user "+this.loggedInUser["data"][0].role);
  //   //return this.loggedInUser;
  // }

  


  // login() {
  //   this.userService.login(this.newUser)
  //   .subscribe((res) => {

  //       // var username = res.data[0].username;
  //       console.log(res["status"])
  //       console.log(res.data[0].role);
  //       localStorage.setItem('loggedinuserid', res.data[0].uid);
  //       localStorage.setItem('loggedinusercmpid', res.data[0].cmpid);
  //       localStorage.setItem('loggedinuserrole', res.data[0].role);
  //       if ( res.data[0].role === 'super' || res.data[0].role === 'superuser') {
  //         this.router.navigate(['super']);
  //       } else if (res.data[0].role === 'cmmadmin') {
  //         this.router.navigate(['cmmaadmin']);
  //       } else {
  //         this.router.navigate(['home']);
  //       }


  //   });

  // }

}

