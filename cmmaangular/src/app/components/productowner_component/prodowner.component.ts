import { Response } from '@angular/http';
import { CompanyService } from '../../services/company.service';
import { DomainService } from '../../services/domain.service';
import { ApplicationService } from '../../services/application.service';
import { UserService } from '../../services/user.service';
import { DomainApplicationUserService } from '../../services/dmappuser.service';
import {AuthenticationService} from '../../services/authentication.service'
import {CmmaService} from '../../services/cmma.service'
import {ProjectService} from '../../services/project.service'
import Company from '../../models/company.model';
import User from '../../models/user.model';
import Domain from '../../models/domain.model';
import DomainApplicationUser from '../../models/dmappuser.model';
import { Component, OnInit,Input } from '@angular/core';
import { Router } from '@angular/router';
import { Variable } from '@angular/compiler/src/render3/r3_ast';
import Application from '../../models/application.model';
import { FormGroup } from '@angular/forms';
import CmmaMapping from '../../models/cmmamapping.model';
import CmmaResult from '../../models/cmmaresult.model';
import CmmaMetadataParameterLevel from '../../models/cmmamdparamlevel.model';
import CompanyProfileCmmaParameterLevel from '../../models/cpprofcmmaparamlevel.model';
import Project from '../../models/project.model';
import { map } from 'rxjs/operators';

@Component({
  // selector: 'app-user-login',
  templateUrl: './po.component.html',

  // styleUrls: ['./app.component.scss']
})
export class ProductOwner implements OnInit {

  showOptions:boolean = true;
  showDashboard:boolean = false;
  showCreateAppUser:boolean = false;
  showFinalDashboard:boolean = false;

  showDomains:boolean = true;
  showApps:boolean = true;
  createShow:boolean = true;
  viewShow:boolean = true;
  isView = true;
  submissionStatus:boolean = false;
  submissionMessage:string = 'Templae already configured. To configure it again, make necessary changes and click create';

  companyName:String;
  domainName:String;
  public newUser: User = new User();
  public finalCmmaResult:CmmaResult = new CmmaResult();
  public dummyCompany: Company = new Company();
  companyList: Company[];
  domainList: Domain[];
  applicationList: Application[];
  mappedApplications: String[];
  dummycompanyList: Company[];
  domainApplicationUsers: DomainApplicationUser
  poForm:FormGroup;
  public companyLogo: String;
  public cmmaMdParamLevelsByCmdmid:CmmaMetadataParameterLevel[];
  public levels:CmmaMetadataParameterLevel[];
  public levelsByParameter:CmmaMetadataParameterLevel = new CmmaMetadataParameterLevel();
  public cmmaMetadataParameterLevel:CmmaMetadataParameterLevel = new CmmaMetadataParameterLevel();
  public cmmaMetadataParameterLevelArray : Array<CmmaMetadataParameterLevel> = new Array<CmmaMetadataParameterLevel>();
  public loggedInUserName: String;
  public selectedMapping: string;
  public selectedProcessArea: String;
  public selectedParameter: string;

  public selectedProject: string;
  public selectedProjectId: number;

  public selectedApplication: string;
  public selectedApplicationId: number;

  public selectedDomain: string;
  public selectedDomainId: number;

  public selectedCmdmid: Number;
  public cmmaMappingsList: CmmaMapping[];
  public projects: Project[];
  public processAreaList: CmmaMapping[];
  public cmmaSet = new Set();
  public companyProfileCmmaParameterLevel:CompanyProfileCmmaParameterLevel = new CompanyProfileCmmaParameterLevel();



  constructor(

    private companyService: CompanyService,
    private domainService: DomainService,
    private applicationService:ApplicationService,
    private domainAppUserService:DomainApplicationUserService,
    private authenticationService:AuthenticationService,
    private userService:UserService,
    private router: Router,
    private cmmaService:CmmaService,
    private projectService:ProjectService

  ) { }




  ngOnInit(): void {

    //this.checkLogIn();

    let loggedInUserCmpId: Number = this.authenticationService.getCurrentUser().cmpid;
    let nameOfCompany:string;

    let rawSet = new Set();

    // Fetch Organisations on initialization
    console.log("Product owner component before companies");
    this.companyService.getCompanies()
    .subscribe(companies => {
      // assign the todolist property to the proper http response
      console.log("Product owner component after companies");

      this.companyList = companies;

      this.companyList.forEach(function(value) {
        console.log("Logged in user cmpid "+loggedInUserCmpId);
        if (value.cmpid === loggedInUserCmpId) {
          console.log(value.cmpid);
          console.log(loggedInUserCmpId);
           nameOfCompany = value.name;
        }

      })

      this.companyName = nameOfCompany;

      console.log('final' + nameOfCompany);
    this.companyName = nameOfCompany;
    this.companyLogo = '../assets/img/'+this.companyName+'.png';
    console.log('companylogo'+ this.companyLogo);


    });

    // Fetch Domains on initialization
    this.domainService.getCompanyDomain(loggedInUserCmpId)
    .subscribe(domains => {
      this.domainList = domains;
    });


    // Fetch CMMA mappings to prepopulate on initialization
    this.cmmaService.getCmmaMappings()
    .subscribe(cmmaMappings => {

      this.cmmaMappingsList = cmmaMappings;

      this.cmmaMappingsList.forEach(function(value){
        if(rawSet.has(value.mapping)){

        }else{
          rawSet.add(value.mapping);
        }
      })
       console.log(rawSet);
    });
      this.cmmaSet = rawSet;

      this.projectService.getProjects()
      .subscribe(projects => {
        this.projects = projects;
      })


  }

  configureCmmatemplate(){

    let domainName = this.domainName;
    let selectedApplication = this.selectedApplication
    let selectedProject = this.selectedProject;
    let dmnid:Number = 0;
    let appid:Number = 0;
    let prjid:Number = 0; 


    this.domainList.forEach(function(value){
      if(value.name === domainName){
          dmnid =  value.dmid;
      }
    })

    this.companyProfileCmmaParameterLevel.dmnid = dmnid;

    this.applicationList.forEach(function(value){
      if(value.name === selectedApplication){
        appid = value.appid;
      }
    })

    this.companyProfileCmmaParameterLevel.appid = appid;

    this.projects.forEach(function(value){
      console.log("Selected projects "+selectedProject +' '+ value.name)
      if(selectedProject === value.name){
        console.log("Selected project ids  "+prjid +' '+ value.prjid)
        prjid = value.prjid;
      }
    })
    
    this.companyProfileCmmaParameterLevel.prjid = prjid;
    console.log("Selected project ids  "+this.companyProfileCmmaParameterLevel.prjid +' '+ prjid)
    this.companyProfileCmmaParameterLevel.uid = this.authenticationService.getCurrentUser().uid; 

    this.cmmaService.createCompanyCmmaDataParameterLevel(this.companyProfileCmmaParameterLevel)
    .subscribe(cpcmmapl =>{
      console.log("Company profile cmma levels "+cpcmmapl+" created successfully.");
    });

  }

  populateProcessArea(): void{
    this.cmmaService.getCmmaMappingsByMapping(this.companyProfileCmmaParameterLevel.mapping)
    .subscribe(cmmaMappings => {
      this.processAreaList = cmmaMappings;
    });
  }

  checkConfigurationStatus(){


    let domainName = this.domainName;
    let selectedApplication = this.selectedApplication
    let selectedProject = this.selectedProject;
    let dmnid:Number = 0;
    let appid:Number = 0;
    let prjid:Number = 0; 

    this.cmmaService.getCompanyCmmaDataParameterLevel(this.companyProfileCmmaParameterLevel.mapping,this.companyProfileCmmaParameterLevel.processarea,this.companyProfileCmmaParameterLevel.parameter)
    .subscribe(res => {
      console.log("In check configuration status"+res)
      if(res["data"].mapping === this.companyProfileCmmaParameterLevel.mapping &&
         res["data"].processarea === this.companyProfileCmmaParameterLevel.processarea &&
         res["data"].parameter === this.companyProfileCmmaParameterLevel.parameter  
      ){

        this.domainList.forEach(function(value){
          if(value.name === domainName){
              dmnid =  value.dmid;
          }
        })
    
        this.companyProfileCmmaParameterLevel.dmnid = dmnid;
    
        this.applicationList.forEach(function(value){
          if(value.name === selectedApplication){
            appid = value.appid;
          }
        })
    
        this.companyProfileCmmaParameterLevel.appid = appid;
    
        this.projects.forEach(function(value){
          if(selectedProject === value.name){
            prjid = value.prjid;
          }
        })
    
        this.companyProfileCmmaParameterLevel.prjid = prjid;

        if(res["data"].uid === this.companyProfileCmmaParameterLevel.uid &&
         res["data"].dmnid === this.companyProfileCmmaParameterLevel.dmnid &&
         res["data"].appid === this.companyProfileCmmaParameterLevel.appid  
      ){
          this.submissionStatus = true;
       }
      }
    });
  } 

  populateLevels(): void{

    let domainName = this.domainName;
    let selectedApplication = this.selectedApplication
    let selectedProject = this.selectedProject;
    let dmnid:Number = 0;
    let appid:Number = 0;
    let prjid:Number = 0; 

    this.cmmaService.getCompanyCmmaDataParameterLevel(this.companyProfileCmmaParameterLevel.mapping,this.companyProfileCmmaParameterLevel.processarea,this.companyProfileCmmaParameterLevel.parameter)
    .subscribe(cplevels => {
      console.log("In check configuration status"+cplevels)

      // console.log(cplevels[0].mapping +' '+ this.companyProfileCmmaParameterLevel.mapping +' '+
      //   cplevels[0].processarea +' '+ this.companyProfileCmmaParameterLevel.processarea +' '+
      //   cplevels[0].parameter +' '+ this.companyProfileCmmaParameterLevel.parameter)



      if(cplevels[0] !== undefined && (cplevels[0].mapping === this.companyProfileCmmaParameterLevel.mapping &&
        cplevels[0].processarea === this.companyProfileCmmaParameterLevel.processarea &&
        cplevels[0].parameter === this.companyProfileCmmaParameterLevel.parameter) 
      ){ 
        
        this.domainList.forEach(function(value){
          if(value.name === domainName){
              dmnid =  value.dmid;
          }
        })
    
        this.companyProfileCmmaParameterLevel.dmnid = dmnid;
    
        this.applicationList.forEach(function(value){
          if(value.name === selectedApplication){
            appid = value.appid;
          }
        })
    
        this.companyProfileCmmaParameterLevel.appid = appid;
    
        this.projects.forEach(function(value){
          if(selectedProject === value.name){
            prjid = value.prjid;
          }
        })
    
        this.companyProfileCmmaParameterLevel.prjid = prjid;
        this.companyProfileCmmaParameterLevel.uid = this.authenticationService.getCurrentUser().uid; 

        console.log(cplevels[0].uid +' '+ this.companyProfileCmmaParameterLevel.uid +' '+
          cplevels[0].dmnid +' '+ this.companyProfileCmmaParameterLevel.dmnid +' '+
          cplevels[0].appid +' '+ this.companyProfileCmmaParameterLevel.appid)

        if(cplevels[0].uid === this.companyProfileCmmaParameterLevel.uid &&
          cplevels[0].dmnid === this.companyProfileCmmaParameterLevel.dmnid &&
          cplevels[0].appid === this.companyProfileCmmaParameterLevel.appid  
      ){
          this.submissionStatus = true;
          console.log('At this point ')

          this.companyProfileCmmaParameterLevel.level1 =  cplevels[0].level1; 
          this.companyProfileCmmaParameterLevel.level2 =  cplevels[0].level2;
          this.companyProfileCmmaParameterLevel.level3 =  cplevels[0].level3; 
          this.companyProfileCmmaParameterLevel.level4 =  cplevels[0].level4;
          this.companyProfileCmmaParameterLevel.level5 =  cplevels[0].level5;

       }
      }else{
        this.submissionStatus = false;

    console.log("Inside populate levels");
    let levelsByParameter:CmmaMetadataParameterLevel = new CmmaMetadataParameterLevel();
    let selectedParameter : String = this.companyProfileCmmaParameterLevel.parameter;
    this.cmmaService.getCmmaParameterLevelsByCmdmid(this.selectedCmdmid)
    
    .subscribe(levels => {
      this.levels = levels;

      this.levels.forEach(function(value){
        console.log(value)
        console.log(selectedParameter)
        if(value.parameter === selectedParameter){
          levelsByParameter = value;
        }
      });
      this.setLevelsByParameter(levelsByParameter);
      console.log("populate parameters"+this.levelsByParameter.level1);
    })
    //this.levelsByParameter = levelsByParameter;
    console.log("End of populate parameters"+this.levelsByParameter);
      }
    });    
  }

  setLevelsByParameter(levelsByParameter:CmmaMetadataParameterLevel){
    this.levelsByParameter = levelsByParameter;

    this.companyProfileCmmaParameterLevel.level1 = this.levelsByParameter.level1;
    this.companyProfileCmmaParameterLevel.level2 = this.levelsByParameter.level2;
    this.companyProfileCmmaParameterLevel.level3 = this.levelsByParameter.level3;
    this.companyProfileCmmaParameterLevel.level4 = this.levelsByParameter.level4;
    this.companyProfileCmmaParameterLevel.level5 = this.levelsByParameter.level5;

    console.log(this.levelsByParameter);
  }

  filterApps(): void{
    console.log("Inside filter apps");
    this.applicationService.getAppsByDomain(this.domainName)
    .subscribe(applications => {
      console.log(applications)
      this.applicationList = applications;
    });
  }


  // Method to populate Parameter dropdown based on selected mapping and process area
  filterParameter(): void{


    console.log('In filter parameter');
    let selectedMapping = this.companyProfileCmmaParameterLevel.mapping
    let selectedProcessArea = this.companyProfileCmmaParameterLevel.processarea
    console.log(selectedProcessArea);
    let selectedCmdmid:Number = 0;

    this.cmmaMappingsList.forEach(function(value){

      if(value.mapping === selectedMapping && value.processarea === selectedProcessArea){

        selectedCmdmid = value.cmdmid;

      }
    })

    this.selectedCmdmid = selectedCmdmid
    console.log("In filter parameter"+this.selectedCmdmid)

    this.cmmaService.getCmmaParameterLevelsByCmdmid(this.selectedCmdmid)
    .subscribe(cmmamdparamlevels => {
      console.log(cmmamdparamlevels);
      this.cmmaMetadataParameterLevelArray = cmmamdparamlevels;
      this.setCmmdParameterLevelsBycmdmid(cmmamdparamlevels);
      console.log("In filter parameter"+this.cmmaMetadataParameterLevelArray)
    });

  }

  setCmmdParameterLevelsBycmdmid(cmmaMetadataParameterLevel:CmmaMetadataParameterLevel[]) {
    this.cmmaMetadataParameterLevelArray = cmmaMetadataParameterLevel;
    console.log("In setCmmdParameterLevelsBycmdmid"+this.cmmaMetadataParameterLevelArray);
  }

 // Method to check status of logged in user.
  // checkLogIn(){
  //   if(this.authenticationService.isLoggedIn()){
  //     this.loggedInUserName = this.authenticationService.getCurrentUser().fullname;
  //   }else{
  //     this.router.navigate(['login']);
  //   }
  // }

  logout(){
    this.authenticationService.logout();
  }

  createProductOwner() {

    let cmpName = this.companyName;
    let domainName = this.domainName;
    let cmpid:Number = 0;
    let dmnid:Number = 0;
    let domainApplicationUser:DomainApplicationUser = new DomainApplicationUser() ;
    let applicationList: Application[] = this.applicationList;
    let mappedApplications: String[] = this.mappedApplications;
    let domainList:Domain[] = this.domainList;
    let domainAppUserService:DomainApplicationUserService = this.domainAppUserService;


    console.log(this.newUser.cmpid)
    console.log(this.companyName)

      this.companyList.forEach( function (value) {
          if (value.name === cmpName) {
            cmpid = value.cmpid;
          }
      });

      this.domainList.forEach( function (value) {
        if (value.name === domainName) {
          dmnid = value.dmid;
        }
    });

      console.log(cmpid)


      console.log('Inside product owner component')
      this.newUser.role = 'productowner';
      this.newUser.createdbyid = parseInt(localStorage.getItem('loggedinuserid'));
      this.newUser.cmpid = cmpid;
      this.userService.create(this.newUser)
      .subscribe((res) => {

        console.log('Inside product owner service response')

        mappedApplications.forEach(function(mappedapplication){

          console.log(mappedapplication);


        applicationList.forEach(function(application){
          console.log(application.name);
          if(mappedapplication === application.name){

            domainApplicationUser.uid = res["data"].uid;
            domainApplicationUser.appid = application.appid;

            domainList.forEach(function(value){
              if(value.name === application.domain){
                domainApplicationUser.dmnid = value.dmid;
              }
            })


            console.log(application.name);
            console.log(mappedapplication);

            domainAppUserService.create(domainApplicationUser)
            .subscribe((res) => {
              console.log("Domain app user created")
            })

          }
        })


        })


      });
  }

  createApplicationUser() {

    let cmpName = this.companyName;
    let domainName = this.domainName;
    let cmpid:Number = 0;
    let dmnid:Number = 0;
    let domainApplicationUser:DomainApplicationUser = new DomainApplicationUser() ;
    let applicationList: Application[] = this.applicationList;
    let mappedApplications: String[] = this.mappedApplications;
    let domainList:Domain[] = this.domainList;
    let domainAppUserService:DomainApplicationUserService = this.domainAppUserService;


    console.log(this.newUser.cmpid)
    console.log(this.companyName)

      this.companyList.forEach( function (value) {
          if (value.name === cmpName) {
            cmpid = value.cmpid;
          }
      });

      this.domainList.forEach( function (value) {
        if (value.name === domainName) {
          dmnid = value.dmid;
        }
    });

      console.log(cmpid)


      console.log('Inside product owner component')      
      this.newUser.createdbyid = this.authenticationService.getCurrentUser().uid;      
      this.newUser.cmpid = cmpid;
      this.userService.create(this.newUser)
      .subscribe((res) => {

        console.log('Inside product owner service response')

        mappedApplications.forEach(function(mappedapplication){

          console.log(mappedapplication);


        applicationList.forEach(function(application){
          console.log(application.name);
          if(mappedapplication === application.name){

            domainApplicationUser.uid = res["data"].uid;
            domainApplicationUser.appid = application.appid;

            domainList.forEach(function(value){
              if(value.name === application.domain){
                domainApplicationUser.dmnid = value.dmid;
              }
            })


            console.log(application.name);
            console.log(mappedapplication);

            domainAppUserService.create(domainApplicationUser)
            .subscribe((res) => {
              console.log("Domain app user created")
            })

          }
        })


        })


      });
  }

  viewCompanyProfile() {
    //CompanyComponent.isView = true;
    this.companyService.getCompanies()
    .subscribe((companies) => {
      console.log('Inside view company profile')
      this.companyList = companies;
      //console.log(CompanyComponent.isView)
      this.router.navigate(['createcp']);
    });

  }

  viewDashboard(){
    this.showOptions = false;
    this.showDashboard = true;
  }

  hideDashboard(){
    this.showOptions = true;
    this.showDashboard = false;
  }

  viewCreateAppUser(){
    this.showOptions = false;
    this.showCreateAppUser = true;
  }

  hideCreateAppUser(){
    this.showOptions = true;
    this.showCreateAppUser = false;
  }

  viewFinalDashboard(){
    this.showOptions = false;
    this.showFinalDashboard = true;
  }

  hideFinalDashboard(){
    this.showOptions = true;
    this.showFinalDashboard = false;
  }

  displayDashboard(){
    console.log('Display')
    this.cmmaService.getCmmaResults(this.domainName,this.selectedApplication,
      this.selectedProject,this.companyProfileCmmaParameterLevel.mapping,this.companyProfileCmmaParameterLevel.processarea
      ,this.companyProfileCmmaParameterLevel.parameter)
    .subscribe(cmmaresult => {
      console.log(cmmaresult)

      this.finalCmmaResult = cmmaresult;

    });
  }

  



}
