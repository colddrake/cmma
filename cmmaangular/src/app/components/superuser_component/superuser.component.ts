import { Response } from '@angular/http';
import { UserService } from '../../services/user.service';
import {CompanyService} from '../../services/company.service'
import {DomainService} from '../../services/domain.service'
import {DomainApplicationUserService} from '../../services/dmappuser.service'
import {AuthenticationService} from '../../services/authentication.service'
import {ApplicationService} from '../../services/application.service'
import {ProjectService} from '../../services/project.service'
import {CmmaService} from '../../services/cmma.service'
import User from '../../models/user.model';
import Company from '../../models/company.model';
import Project from '../../models/project.model'
import Domain from '../../models/domain.model';
import Application from '../../models/application.model';
import DomainApplicationUser from '../../models/dmappuser.model';
import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { Variable } from '@angular/compiler/src/render3/r3_ast';
import {CompanyComponent} from '../company_component/company.component'
import { Console } from '@angular/core/src/console';
import { FormGroup } from '@angular/forms';
import CmmaMapping from '../../models/cmmamapping.model';
import CmmaMetadataParameterLevel from '../../models/cmmamdparamlevel.model';
import { map } from 'rxjs/operators';

@Component({
  // selector: 'app-user-login',Model)]="cmmaMetadataParameterLevel.level1"
  templateUrl: './superuser.component.html'
  // styleUrls: ['./app.component.scss']
})
export class SuperUserComponent implements OnInit {

  companyProfile:number;

  constructor(
    // Private todoservice will be injected into the component by Angular Dependency Injector
    private userService: UserService,    
    private companyService: CompanyService,
    private router: Router,
    private companyComponent:CompanyComponent,
    private applicationService:ApplicationService,
    private projectService:ProjectService,
    private domainService: DomainService,
    private authenticationService:AuthenticationService,
    private domainAppUserService:DomainApplicationUserService,
    private cmmaService:CmmaService

  ) { }

  companyList: Company[];
  
  

  public newUser: User = new User();
  public companyName: String;
  public companyLogo: String;
  public loggedInUser: String = localStorage.getItem("loggedinuserid");  
  public loggedInUserName: String;

  public processAreaList: CmmaMapping[];
  public newCmmaMetadataMapping: CmmaMapping = new CmmaMapping();  
  public cmmaMappingsList: CmmaMapping[];
  public cmmaMdParamLevels:CmmaMetadataParameterLevel[];
  public cmmaMdParamLevelsByCmdmid:CmmaMetadataParameterLevel[];
  public cmmaMetadataParameterLevel:CmmaMetadataParameterLevel = new CmmaMetadataParameterLevel();
  public selectedMapping: string;
  public selectedProcessArea: String;
  public selectedParameter: string;
  public otherMapping: string;
  public otherProcessArea: string;
  public otherParameter: string;
  public selectedCmdmid: Number;
  public cmmaSet = new Set();
  


  showOptions:boolean = true;
  viewCrePo:boolean = false;
  viewCrePrj:boolean =false;
  viewCrecmmatep:boolean =false;
  showDomains:boolean = true;
  showApps:boolean = true;
  createShow:boolean = true;
  createPoShow:boolean = true;
  viewShow:boolean = true;
  showOthers:boolean = false;
  showOtherMappings:boolean = false;
  showOtherParameters:boolean = false;
  showOtherProcessAreas:boolean = false;  


  domainName:String;  
  public dummyCompany: Company = new Company();
  public newProject: Project = new Project();
  
  domainList: Domain[];
  applicationList: Application[];
  mappedApplications: String[];
  isView = true;
  dummycompanyList: Company[];
  projectList: Project[];
  domainApplicationUsers: DomainApplicationUser[];
  poForm:FormGroup;
  

  ngOnInit(): void {// At component initialization the

    this.checkLogIn();
    let loggedInUserCmpId: Number = this.authenticationService.getCurrentUser().cmpid; 
    let nameOfCompany:string;
    let rawSet = new Set();

    this.companyService.getCompanies()
    .subscribe(companies => {
      // assign the todolist property to the proper http response
      this.companyList = companies;

      this.companyList.forEach(function(value) {

        if (value.cmpid === loggedInUserCmpId) {
          console.log(value.cmpid);
          console.log(loggedInUserCmpId);
           nameOfCompany = value.name;
        }

      })

      console.log('final' + nameOfCompany);
      this.companyName = nameOfCompany;
      this.companyLogo = '../assets/img/'+this.companyName+'.png';
      console.log('companylogo'+ this.companyLogo);

    });

    this.projectService.getProjects()
    .subscribe(projects => {
      this.projectList = projects;
    })

    this.domainService.getCompanyDomain(this.authenticationService.getCurrentUser().cmpid)
    .subscribe(domains => {      
      this.domainList = domains;              
    });

    this.cmmaService.getCmmaMappings()
    .subscribe(cmmaMappings => {      
      
      this.cmmaMappingsList = cmmaMappings;

      this.cmmaMappingsList.forEach(function(value){
        console.log(value);
      })
      
      this.cmmaMappingsList.forEach(function(value){
        if(rawSet.has(value.mapping)){

        }else{
          rawSet.add(value.mapping);
        }
      })

      rawSet.forEach(function(value){
        console.log(value)
      })
       
    });
      this.cmmaSet = rawSet;

  }

  filterApps(): void{
    this.applicationService.getAppsByDomain(this.domainName)
    .subscribe(applications => {
      console.log(applications)
      this.applicationList = applications;
    });
  }

  checkLogIn(){
    if(this.authenticationService.isLoggedIn()){
      this.loggedInUserName = this.authenticationService.getCurrentUser().fullname;
    }else{
      this.router.navigate(['login']);
    }
  }
   
  logout(){
    this.authenticationService.logout();
  }



  create() {
    let cmpName = this.companyName;
    let cmpid:Number = 0;

    console.log(this.newUser.cmpid)
    console.log(this.companyName)

      this.companyList.forEach( function (value) {
          if (value.name === cmpName) {
            cmpid = value.cmpid;
          }
      });

      console.log(cmpid)


      console.log('Inside super component user')
      this.newUser.role = 'superuser';
      this.newUser.createdbyid = parseInt(localStorage.getItem('loggedinuserid'));
      this.newUser.cmpid = cmpid;
      this.userService.create(this.newUser)
      .subscribe((res) => {
        console.log('Inside super component user service response')
      });

    }

    createProductOwner() {    

      let cmpName = this.companyName;
      let domainName = this.domainName;
      let cmpid:Number = 0;
      let dmnid:Number = 0;
      let domainApplicationUser:DomainApplicationUser = new DomainApplicationUser() ;
      let applicationList: Application[] = this.applicationList;
      let mappedApplications: String[] = this.mappedApplications;
      let domainList:Domain[] = this.domainList;
      let domainAppUserService:DomainApplicationUserService = this.domainAppUserService;
      
  
      console.log(this.newUser.cmpid)
      console.log(this.companyName)
  
        this.companyList.forEach( function (value) {
            if (value.name === cmpName) {
              cmpid = value.cmpid;            
            }
        });
  
        this.domainList.forEach( function (value) {
          if (value.name === domainName) {
            dmnid = value.dmid;
          }
      });
  
        console.log(cmpid)
  
  
        console.log('Inside product owner component')
        this.newUser.role = 'productowner';
        this.newUser.createdbyid = parseInt(localStorage.getItem('loggedinuserid'));
        this.newUser.cmpid = cmpid;      
        this.userService.create(this.newUser)
        .subscribe((res) => {
  
          console.log('Inside product owner service response')
  
          mappedApplications.forEach(function(mappedapplication){
            
            console.log(mappedapplication);
              
      
          applicationList.forEach(function(application){
            console.log(application.name);
            if(mappedapplication === application.name){
  
              domainApplicationUser.uid = res["data"].uid;
              domainApplicationUser.appid = application.appid;
  
              domainList.forEach(function(value){
                if(value.name === application.domain){
                  domainApplicationUser.dmnid = value.dmid;
                }
              })
              
  
              console.log(application.name);
              console.log(mappedapplication);
              
              domainAppUserService.create(domainApplicationUser)
              .subscribe((res) => {
                console.log("Domain app user created")
              })           
  
            }
          })
      
      
          })
  
  
        });
    }

  createProject(){
      console.log(this.newProject.description)
      this.projectService.create(this.newProject)
      .subscribe((res) => {

      });
  }

  populateProcessArea(): void{
    console.log("In populate process area");
    this.cmmaService.getCmmaMappingsByMapping(this.selectedMapping)
    .subscribe(cmmaMappings => {
      this.processAreaList = cmmaMappings;
    });
  }

  filterParameter(): void{

    //console.log(this.selectedMapping+'   '+this.selectedProcessArea);

    let selectedMapping = this.selectedMapping;
    let selectedProcessArea = this.selectedProcessArea
    let selectedCmdmid:Number = 0;
    //console.log("In filter parameter"+this.cmmaMappingsList);
    this.cmmaMappingsList.forEach(function(value){
      //console.log("In filter parameter value mapping"+value.mapping)
      //console.log("In filter parameter selected mapping"+selectedMapping)
      //console.log("In filter parameter value process area"+value.processarea)
      //console.log("In filter parameter selected process area"+selectedProcessArea)
      if(value.mapping === selectedMapping && value.processarea === selectedProcessArea){
        //console.log("In filter parameter value cmdmid"+value.cmdmid);
        selectedCmdmid = value.cmdmid;
        //console.log("In filter parameter selected cmdmid"+selectedCmdmid);
      }
    })

    this.selectedCmdmid = selectedCmdmid
    console.log("In filter parameter"+this.selectedCmdmid)
    this.cmmaService.getCmmaParameterLevelsByCmdmid(this.selectedCmdmid)
    .subscribe(cmmamdparamlevels => {
      this.cmmaMdParamLevelsByCmdmid = cmmamdparamlevels;
    });
    console.log("In filter parameter"+this.cmmaMdParamLevelsByCmdmid)
    console.log("****************************************")
  }

  createCmmaMetadataParameterLevel(){
    
    //let cmdmid : Number = 0;
    let cmdmidOfParameter : Number = 0;

    if(this.otherMapping !== undefined && this.otherProcessArea !== undefined && this.otherParameter !== undefined){
      
      this.cmmaMetadataParameterLevel.parameter = this.otherParameter;

      this.newCmmaMetadataMapping.mapping = this.otherMapping;
      this.newCmmaMetadataMapping.processarea = this.otherProcessArea;

      // console.log("Mapping"+this.newCmmaMetadataMapping);

      

       this.cmmaService.createCmmaMappingMeatada(this.newCmmaMetadataMapping)
       .subscribe(cmmaMapping => {
         
         console.log(cmmaMapping.cmdmid);     
         cmdmidOfParameter = cmmaMapping.cmdmid;
         console.log(cmdmidOfParameter);  
       this.cmmaMetadataParameterLevel.cmdmid = cmmaMapping.cmdmid;;

       console.log(this.cmmaMetadataParameterLevel.cmdmid);

       this.createCmmaParameterLevel(this.cmmaMetadataParameterLevel);

       });     

    }else{

      console.log(this.selectedCmdmid);

      if(this.selectedParameter !== undefined){
        this.cmmaMetadataParameterLevel.parameter = this.selectedParameter;
      }else{
        this.cmmaMetadataParameterLevel.parameter = this.otherParameter;
      }
      
      this.cmmaMetadataParameterLevel.cmdmid = this.selectedCmdmid;

      console.log(this.cmmaMetadataParameterLevel.parameter);
      console.log(this.cmmaMetadataParameterLevel.cmdmid);

      this.createCmmaParameterLevel(this.cmmaMetadataParameterLevel);  


    }    
    
  }

  createCmmaParameterLevel(cmmaMetadataParameterLevel){

    console.log(cmmaMetadataParameterLevel.cmdmid);

    this.cmmaService.createCmmaMeatadaParameterLevel(cmmaMetadataParameterLevel)
       .subscribe(cmmaMetadataParameterLevel => {
          console.log(cmmaMetadataParameterLevel.cmdmid);
          console.log(cmmaMetadataParameterLevel.parameter);
          console.log(cmmaMetadataParameterLevel.level1);
          console.log(cmmaMetadataParameterLevel.level2);
          console.log(cmmaMetadataParameterLevel.level3);
          console.log(cmmaMetadataParameterLevel.level4);
          console.log(cmmaMetadataParameterLevel.level5);    
       });
  }

  getProjects(){
    this.projectService.getProjects()
    .subscribe((res) => {

    });
}

    // toProductOwner() {
    //   this.router.navigate(['po']);
    // }

    toApplicationDomain() {
      this.router.navigate(['domain']);
    }

    toApplication() {
      this.router.navigate(['app']);
    }

    createPo(){
      this.showOptions = false;
      this.viewCrePo = true;
    }

    hidePo(){
      this.showOptions = true;
      this.viewCrePo = false;
    }

    createPrj(){
      this.showOptions = false;
      this.viewCrePrj = true;
    }

    hidePrj(){
      this.showOptions = true;
      this.viewCrePrj = false;
    }

    createCmmaTemp(){
      this.showOptions = false;
      this.viewCrecmmatep = true;
    }

    hideCmmaTemp(){
      this.showOptions = true;
      this.viewCrecmmatep = false;
    }

    displayOtherParameter(){
      this.showOthers = true;
      this.showOtherParameters = true;
    }

    displayOtherProcessArea(){
      this.showOthers = true;
      this.showOtherProcessAreas  =true;
    }

    displayOtherMapping(){
      this.showOthers = true;
      this.showOtherMappings = true;
    }


}

