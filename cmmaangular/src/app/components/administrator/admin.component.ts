import { Response } from '@angular/http';
import {UserService} from '../../services/user.service';
import User from '../../models/user.model';
import UserActivities from '../../models/useractivities.model';
import Company from '../../models/company.model';
import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import {CompanyComponent} from '../company_component/company.component'
import {CompanyService} from '../../services/company.service'
import {AuthenticationService} from '../../services/authentication.service'
import { AlertsService } from 'angular-alert-module';
import {bloomFindPossibleInjector} from '@angular/core/src/render3/di';
import {NgForm} from '@angular/forms';

@Component({
  // selector: 'app-user-login',
   templateUrl: './admin.component.html'
   
   
  // styleUrls: ['./app.component.scss']
})
export class Administrator implements OnInit {
  
  show : boolean = true;
  createSuShow : boolean = true;
  viewSuShow : boolean = true;
  createAdminShow : boolean = true;
  viewAdminShow : boolean = true;
  suview : boolean = true;
  suEdit : boolean = true;
  success: boolean;
  failuer: boolean;
  

  companyList: Company[];  
  superuserList: User[];
  adminList: User[];
  activeUser: User[];
  selectedSuperUser: String;

  public newUser: User = new User();
  public viewUser: User = new User();  
  public userActivities:UserActivities = new UserActivities();
  public companyName: String;
  public companyLogo: String;
  public suCompanyName: String;
  public loggedInUserName: String;
  public selectedAdmin: String;
  public date:Date = new Date();

  constructor(
    
    private userService: UserService,
    private companyService: CompanyService,
    private authenticationService: AuthenticationService,
    private router: Router,
    private alertService:AlertsService,
    private companyComponent:CompanyComponent
  ) { }

  


  ngOnInit(): void {

    //this.checkLogIn();

    //let loggedInUserCmpId: number = parseInt(localStorage.getItem("loggedinusercmpid")); 
    console.log("In administrator ngOnIt "+this.authenticationService.getCurrentUser());
    console.log("In administrator ngOnIt "+this.authenticationService.getCurrentUser().cmpid);
  

    var loggedInUserCmpId: Number = this.authenticationService.getCurrentUser().cmpid; 
    let nameOfCompany:string;

    this.companyService.getCompanies()
    .subscribe(companies => {
      // assign the todolist property to the proper http response     

      this.companyList = companies;               

      this.companyList.forEach(function(value) {

        if (value.cmpid === loggedInUserCmpId) {
          console.log(value.cmpid);
          console.log(loggedInUserCmpId);
           nameOfCompany = value.name;
        }

      })

      console.log("In administrator ngOnIt "+loggedInUserCmpId);
      console.log("In administrator ngOnIt "+nameOfCompany);

      this.companyName = nameOfCompany;
      this.companyLogo = '../assets/img/'+this.companyName+'.png';  
      

    });

    this.userService.getUsersByRole('superuser')
      .subscribe(superusers => {
      // assign the todolist property to the proper http response     
      console.log('Super user list '+superusers)
      this.superuserList = superusers["data"];          
      console.log('Super user list '+this.superuserList)

      this.superuserList.forEach(function(value){
        console.log(value.fullname);
      })

    });

    

    this.userService.getUsersByRole('cmmaadmin')
    .subscribe(cmmadmins => {
      // assign the todolist property to the proper http response     

      this.adminList = cmmadmins["data"];    
      
      this.adminList.forEach(function(value){

        console.log('Administrator list '+value);
      })

      

    });

    
  }

  populateDashboard(){    
  }

    administratorView(){
          //adminView();
    }

    deleteAdmin(){
      if(confirm("This will delete user permanantly. Are you sure to proceed?")){
        
      }
    }

    // checkLogIn(){
    //   if(this.authenticationService.isLoggedIn()){
    //     this.loggedInUserName = this.authenticationService.getCurrentUser().fullname;
    //   }else{
    //     this.router.navigate(['login']);
    //   }
    // } 

    toCompanyProfile() {                
      localStorage.setItem("createCompanyProfile","true")
      this.router.navigate(['createcp']);            
    } 

    logout(){
      this.authenticationService.logout();
    }

    // viewAdmin() {    
    //   let selectedUser:String = this.selectedAdmin;
    //   let user:User;
    //   let userList:User[] = this.adminList;
      
    //        userList.forEach(function(value){
    //        if(value.fullname === selectedUser){
    //          user = value;
    //        }
    //    })
      
    //   this.viewUser = user;

    //   let organisations:Company[] = this.companyList;
    //   let organisationName:String;
    //   organisations.forEach(function(value){
    //     if(value.cmpid === user.cmpid){
    //       organisationName = value.name;
    //     }
    //   })

    //   this.suCompanyName = organisationName;
    //   console.log('Org'+this.suCompanyName)
      
    // }

    viewSuperUser() {    
      let selectedUser:String = this.selectedSuperUser;
      let user:User;
      let userList:User[] = this.superuserList;
      
           userList.forEach(function(value){
           if(value.fullname === selectedUser){
             user = value;
           }
       })
      
      this.viewUser = user;

      let organisations:Company[] = this.companyList;
      let organisationName:String;
      organisations.forEach(function(value){
        if(value.cmpid === user.cmpid){
          organisationName = value.name;
        }
      })

      this.suCompanyName = organisationName;
      console.log('Org'+this.suCompanyName)
      
    }

    viewCompanyProfile(companylist) {    
      console.log('Inside super component')
      console.log(companylist)
      localStorage.setItem("isCompanyView","true")
      //this.companyComponent.isView = true;
      this.companyComponent.viewCompanyProfile()
    }

    toSuperUser(){      
      localStorage.setItem("createsuperuser","1");
      this.router.navigate(['super']);
    }
    
    onSubmit(adminForm, userForm: NgForm) {
      adminForm.resetForm();
      userForm.resetForm();
    }

    formSubmit() {
      let successDiv = document.createElement('div');
      let successText = document.createTextNode("Sucessfully submitted");
      successDiv.appendChild(successText);
      successDiv.className = 'submit-success';
      successDiv.id = 'successMessage';
      document.getElementsByClassName('success-div')[0].appendChild(successDiv);
      setTimeout (() => {document.getElementById('successMessage').style.display='none'}, 5000);
    }
    
    createAdministrator() {    
      //CompanyComponent.isView = false;

    let cmpName = this.companyName
    let cmpid:Number = 0;
    

    console.log(this.newUser.cmpid)
    console.log(this.companyName)

      this.companyList.forEach( function (value) {
        console.log(value.name)
          if(value.name === 'Cognizant Technology Solutions' || value.name === 'Cognizant'){
            cmpid = value.cmpid;
          }
      })

      console.log(cmpid)
      

      console.log("Inside administrator")
      this.newUser.role = 'cmmaadmin';
      //this.newUser.createdbyid = parseInt(localStorage.getItem("loggedinuserid"));
      this.newUser.createdbyid = this.authenticationService.getCurrentUser().uid;
      this.newUser.cmpid = cmpid;
      this.newUser.createdDate = this.date;
      this.newUser.updatedDate = this.date;
      this.userService.create(this.newUser)
      .subscribe((res) => {
        console.log("Inside super administrator service response")
        if(res["status"] === 201){          
        }else{
          this.failuer = true;
        }
      });
  
    }

    instantiateUser(){
      this.newUser = new User();
    }

    createSuperUser() {
      // CompanyComponent.isView = false;

    let cmpName = this.companyName;
    let cmpid:Number = 0;

    console.log(this.newUser.cmpid)
    console.log(this.companyName)

      this.companyList.forEach( function (value) {
          if (value.name === cmpName) {
            cmpid = value.cmpid;
          }
      });

      console.log(cmpid)


      console.log('Inside super component user')
      this.newUser.role = 'superuser';
      //this.newUser.createdbyid = parseInt(localStorage.getItem('loggedinuserid'));      
      this.newUser.createdbyid = this.authenticationService.getCurrentUser().uid;
      this.newUser.cmpid = cmpid;
      this.newUser.createdDate = this.date;
      this.newUser.updatedDate = this.date;
      console.log("Inside create superuser "+this.newUser.cmpid+this.newUser.contactnumber+this.newUser.createdbyid+this.newUser.email+this.newUser.fullname+this.newUser.password+this.newUser.role+this.newUser.username);
      this.userService.create(this.newUser)
      .subscribe((res) => {
        console.log('Inside super component user service response')
        var success:boolean;
        if(res["status"] === 201){
          this.success = true;          
        }else{
          this.failuer = true;
        }


      });

    }
    

}

