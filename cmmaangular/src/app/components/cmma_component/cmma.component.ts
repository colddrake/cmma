import { Response } from '@angular/http';
import { CompanyService } from '../../services/company.service';
import { DomainService } from '../../services/domain.service';
import { ApplicationService } from '../../services/application.service';
import { UserService } from '../../services/user.service';
import { DomainApplicationUserService } from '../../services/dmappuser.service';
import {AuthenticationService} from '../../services/authentication.service'
import Company from '../../models/company.model';
import User from '../../models/user.model';
import Domain from '../../models/domain.model';
import DomainApplicationUser from '../../models/dmappuser.model';
import { Component, OnInit,Input } from '@angular/core';
import { Router } from '@angular/router';
import { Variable } from '@angular/compiler/src/render3/r3_ast';
import Application from '../../models/application.model';
import { FormGroup } from '@angular/forms';

@Component({
  // selector: 'app-user-login',
  templateUrl: './cmma.component.html',
  
  // styleUrls: ['./app.component.scss']
})
export class Cmma implements OnInit {
  
  showOptions:boolean = true;
  showDashboard:boolean = false;
  
  showDomains:boolean = true;
  showApps:boolean = true;
  createShow:boolean = true;
  viewShow:boolean = true;
  isView = true;

  companyName:String;
  domainName:String;
  public newUser: User = new User();
  public dummyCompany: Company = new Company();
  companyList: Company[];
  domainList: Domain[];
  applicationList: Application[];
  mappedApplications: String[];  
  dummycompanyList: Company[];
  domainApplicationUsers: DomainApplicationUser  
  poForm:FormGroup;
  public companyLogo: String
  public loggedInUserName: String;
  

  constructor(
    
    private companyService: CompanyService,    
    private domainService: DomainService,
    private applicationService:ApplicationService,
    private domainAppUserService:DomainApplicationUserService,
    private authenticationService:AuthenticationService,
    private userService:UserService,
    private router: Router    
    
  ) { }

  
 
   
  ngOnInit(): void {

    
    this.checkLogIn();

    let loggedInUserCmpId: Number = this.authenticationService.getCurrentUser().cmpid; 
    let nameOfCompany:string;

    this.companyService.getCompanies()
    .subscribe(companies => {
      // assign the todolist property to the proper http response     

      this.companyList = companies;               

      this.companyList.forEach(function(value) {

        if (value.cmpid === loggedInUserCmpId) {
          console.log(value.cmpid);
          console.log(loggedInUserCmpId);
           nameOfCompany = value.name;
        }

      })

      this.companyName = nameOfCompany;

      console.log('final' + nameOfCompany);
    this.companyName = nameOfCompany;
    this.companyLogo = '../assets/img/'+this.companyName+'.png';
    console.log('companylogo'+ this.companyLogo);
      

    });

    this.domainService.getCompanyDomain(loggedInUserCmpId)
    .subscribe(domains => {      
      this.domainList = domains;              
    });    
    

    

    

    
  }
  
  filterApps(): void{
    this.applicationService.getAppsByDomain(this.domainName)
    .subscribe(applications => {
      console.log(applications)
      this.applicationList = applications;
    });
  }

  checkLogIn(){
    // if(this.authenticationService.isLoggedIn()){
    //   this.loggedInUserName = this.authenticationService.getCurrentUser().fullname;
    // }else{
    //   this.router.navigate(['login']);
    // }
  }
   
  logout(){
    this.authenticationService.logout();
  }

  createProductOwner() {    

    let cmpName = this.companyName;
    let domainName = this.domainName;
    let cmpid:Number = 0;
    let dmnid:Number = 0;
    let domainApplicationUser:DomainApplicationUser = new DomainApplicationUser() ;
    let applicationList: Application[] = this.applicationList;
    let mappedApplications: String[] = this.mappedApplications;
    let domainList:Domain[] = this.domainList;
    let domainAppUserService:DomainApplicationUserService = this.domainAppUserService;
    

    console.log(this.newUser.cmpid)
    console.log(this.companyName)

      this.companyList.forEach( function (value) {
          if (value.name === cmpName) {
            cmpid = value.cmpid;            
          }
      });

      this.domainList.forEach( function (value) {
        if (value.name === domainName) {
          dmnid = value.dmid;
        }
    });

      console.log(cmpid)


      console.log('Inside product owner component')
      this.newUser.role = 'productowner';
      this.newUser.createdbyid = parseInt(localStorage.getItem('loggedinuserid'));
      this.newUser.cmpid = cmpid;      
      this.userService.create(this.newUser)
      .subscribe((res) => {

        console.log('Inside product owner service response')

        mappedApplications.forEach(function(mappedapplication){
          
          console.log(mappedapplication);
            
    
        applicationList.forEach(function(application){
          console.log(application.name);
          if(mappedapplication === application.name){

            domainApplicationUser.uid = res["data"].uid;
            domainApplicationUser.appid = application.appid;

            domainList.forEach(function(value){
              if(value.name === application.domain){
                domainApplicationUser.dmnid = value.dmid;
              }
            })
            

            console.log(application.name);
            console.log(mappedapplication);
            
            domainAppUserService.create(domainApplicationUser)
            .subscribe((res) => {
              console.log("Domain app user created")
            })           

          }
        })
    
    
        })


      });
  }

  viewCompanyProfile() {    
    //CompanyComponent.isView = true;
    this.companyService.getCompanies()
    .subscribe((companies) => {        
      console.log('Inside view company profile')      
      this.companyList = companies;
      //console.log(CompanyComponent.isView)      
      this.router.navigate(['createcp']);
    });   

  }

  viewDashboard(){
    this.showOptions = false;
    this.showDashboard = true;
  }

  hideDashboard(){
    this.showOptions = true;
    this.showDashboard = false;
  }

  

}

