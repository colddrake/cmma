import User from '../models/user.model';
import { Observable, pipe } from 'rxjs';
import { HttpClient, HttpErrorResponse } from '@angular/common/http';
import {Response} from '@angular/http';
import { Injectable } from '@angular/core';
import { map } from 'rxjs/operators';
import {AuthenticationService} from './authentication.service' 
import {CookieService} from 'ngx-cookie-service';
  

// RxJS operator for mapping the observable
// import 'rxjs/add/operator/map';

@Injectable()
export class UserService {

  cookieValue = 'UNKNOWN';

  api_url = 'http://localhost:3000';
  userUrl = `${this.api_url}/api/user`;
  registerurl = `${this.api_url}/api/register`;
  loginurl = `${this.api_url}/api/login`;

  constructor(
    private http: HttpClient,
    private authentiationService:AuthenticationService,
    private cookieService:CookieService
  ) { }

  


  userValidationAndRedirections(): Observable<User>{
    return this.http.get(`${this.userUrl}`).
    pipe(map((res:any)=>{      
      // console.log("Res "+res.role);
      // console.log("Res "+res["data"].role);
      console.log("Res "+res["data"][0].role);
      return res["data"][0] as User;
    }))

  }  

  login(user: User): Observable<any> {    
    return this.http.post(`${this.loginurl}`, user).
    pipe(map((res:any) => {

      if(res.status === 201){
        //this.authentiationService.currentUserDetails(res["data"].authToken);
        var date:Date = new Date();
        console.log("Inside login "+date);
        date.setMinutes(date.getMinutes()+30);
        console.log("Inside login "+date);
        

        
                 
        this.cookieService.set('token',res["data"].authToken,date);
        
        return true;
      }else{
        return false;
      }

      // this.authentiationService.setToken(res["token"]);
      // return res["token"];
    }))
    // .subscribe((res)=>{
    //     this.authentiationService.setToken(res["token"]);
    // });
    
  }

      

  // login(user: User):void  {    
  //   this.http.post(`${this.loginurl}`, user)
  //   .subscribe((res)=>{
  //       this.authentiationService.setToken(res["token"]);
        
  //   });
  // }

  logout(user: User): void {        
    //this.authentiationService.deleteToken();    
    //this.cookieService.delete('token');

  }

  create(user: User): Observable<any> {
    return this.http.post(`${this.registerurl}/`, user);
  }

  getUsersByRole(role: String): Observable<any> {
    return this.http.get(`${this.userUrl}/?role=`+role)
    // pipe(map((res:any)  => {
    //   // Maps the response object sent from the server
    //   this.subject.next({type:'success',text:res["data"]});
    //   return this.subject.asObservable();
      //return res["data"] as User[];
    }

  

  getUsersById(token: String): Observable<any> {
    return this.http.get(`${this.userUrl}/?token=`+token).    
    pipe(map((res:any)  => {
      // Maps the response object sent from the server
      
        return res["data"][0] as User;
      
    }))

  }




  
}
