import Company from '../models/company.model';
import CmmaMapping from '../models/cmmamapping.model';
import CmmaMetadataParameterLevel from '../models/cmmamdparamlevel.model';
import CompanyProfileCmmaParameterLevel from '../models/cpprofcmmaparamlevel.model';
import CmmaResult from '../models/cmmaresult.model';
import { Observable } from 'rxjs';
import { HttpClient, HttpErrorResponse } from '@angular/common/http';
import {Response} from '@angular/http';
import { Injectable } from '@angular/core';
import { map } from 'rxjs/operators';
import {AuthenticationService} from './authentication.service'

// RxJS operator for mapping the observable
// import 'rxjs/add/operator/map';

@Injectable()
export class CmmaService {

  api_url = 'http://localhost:3000';
  cmmaMappingUrl = `${this.api_url}/api/cmmamapping`;
  cmmaMdParamLevelUrl = `${this.api_url}/api/cmmaparameterlevel`;
  cpCmmaParamLevelUrl = `${this.api_url}/api/cpcmmadata`;
  cmmaResultUrl = `${this.api_url}/api/cmmaresult`;

  cpcmmadata

  constructor(
    private http: HttpClient,
    private authenticationService:AuthenticationService
  ) { }

  createCmmaResult(cmmaResult:CmmaResult): Observable<CmmaResult>{
    return this.http.post(this.cmmaResultUrl,cmmaResult).
    pipe(map((res:any) => {
      return res["data"] as CmmaResult;
    }))
  }


  createCmmaMappingMeatada(cmmaMapping:CmmaMapping): Observable<CmmaMapping>{
    return this.http.post(this.cmmaMappingUrl,cmmaMapping).
    pipe(map((res:any) => {
      return res["data"] as CmmaMapping;
    }))
  }

  createCmmaMeatadaParameterLevel(cmmaMetadataParameterLevel:CmmaMetadataParameterLevel): Observable<CmmaMetadataParameterLevel>{
    return this.http.post(this.cmmaMdParamLevelUrl,cmmaMetadataParameterLevel).
    pipe(map((res:any) => {
      return res["data"] as CmmaMetadataParameterLevel;
    }))
  }

  createCompanyCmmaDataParameterLevel(companyProfileCmmaParameterLevel:CompanyProfileCmmaParameterLevel): Observable<CompanyProfileCmmaParameterLevel>{
    return this.http.post(this.cpCmmaParamLevelUrl,companyProfileCmmaParameterLevel).
    pipe(map((res:any) => {
      return res["data"] as CompanyProfileCmmaParameterLevel;
    }))
  }

  getCompanyCmmaDataParameterLevel(mapping:String,processarea:String,parameter:String): Observable<CompanyProfileCmmaParameterLevel[]>{
    return this.http.get(this.cpCmmaParamLevelUrl+'?mapping='+mapping+'&processarea='+processarea+'&parameter='+parameter,).
    pipe(map((res:any) => {      
      console.log('In service'+res)
      return res["data"] as CompanyProfileCmmaParameterLevel[];
    }))
  }

  getCmmaMappings(): Observable<CmmaMapping[]>{    
    
    return this.http.get(this.cmmaMappingUrl).
    pipe(map((res:any)  => {
      // Maps the response object sent from the server

      return res["data"].docs as CmmaMapping[];
    }))
  }

  getCmmaMappingsByMapping(mapping:String): Observable<CmmaMapping[]>{    
    
    return this.http.get(this.cmmaMappingUrl+'?mapping='+mapping).
    pipe(map((res:any)  => {
      // Maps the response object sent from the server

      return res["data"] as CmmaMapping[];
    }))
  }

  getCmmaParameterLevels(): Observable<CmmaMetadataParameterLevel[]>{    
    
    return this.http.get(this.cmmaMdParamLevelUrl).
    pipe(map((res:any)  => {      

      return res["data"].docs as CmmaMetadataParameterLevel[];
    }))
  }

  getCmmaParameterLevelsByCmdmid(cmdmid:Number): Observable<CmmaMetadataParameterLevel[]>{    
    
    return this.http.get(this.cmmaMdParamLevelUrl+'?cmdmid='+cmdmid).
    pipe(map((res:any)  => {      

      return res["data"] as CmmaMetadataParameterLevel[];
    }))
  }

  getCmmaResults(domain,application,project,mapping,processarea,parameter): Observable<CmmaResult>{    
    
    return this.http.get(this.cmmaResultUrl+'?domainname='+domain+'&appname='+application+'&project='+project+'&mapping='+mapping+'&processarea='+processarea+'&parameter='+parameter).
    pipe(map((res:any)  => {      
      console.log(res);
      return res["data"][0] as CmmaResult;
    }))
  }

  
}
