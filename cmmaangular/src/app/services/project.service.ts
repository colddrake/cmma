import User from '../models/user.model';
import Project from '../models/project.model';
import { Observable } from 'rxjs';
import { HttpClient, HttpErrorResponse } from '@angular/common/http';
import {Response} from '@angular/http';
import { Injectable } from '@angular/core';
import { map } from 'rxjs/operators';

  

// RxJS operator for mapping the observable
// import 'rxjs/add/operator/map';

@Injectable()
export class ProjectService {

  api_url = 'http://localhost:3000';
  projectUrl = `${this.api_url}/api/project`;
  

  constructor(

    private http: HttpClient    

  ) { }
  

  create(project: Project): Observable<any> {
    return this.http.post(`${this.projectUrl}/`, project);
  }

  getProjects(): Observable<any> {
    return this.http.get(`${this.projectUrl}/`).    
    pipe(map((res:any)  => {
      console.log(res);
      return res["data"].docs as Project[];
    }))

  }


  
}
