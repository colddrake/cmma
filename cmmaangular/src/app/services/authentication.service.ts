import Application from '../models/application.model';
import { Observable } from 'rxjs';
import { HttpClient, HttpErrorResponse } from '@angular/common/http';
import {Response} from '@angular/http';
import { Router } from '@angular/router';
import { Injectable } from '@angular/core';
import { map } from 'rxjs/operators';
import User from '../models/user.model'
import {CookieService} from 'ngx-cookie-service';

// RxJS operator for mapping the observable
// import 'rxjs/add/operator/map';

@Injectable()
export class AuthenticationService {

  api_url = 'http://localhost:3000';
  appUrl = `${this.api_url}/app/app`;

  constructor(
    private http: HttpClient,
    private cookieService:CookieService,
    private router: Router,
  ) { }

  public loggedInUser: User = new User();
  public currentUser: User = new User();


  setToken(token:string): void {
    localStorage.setItem("token",token);
  }

  deleteToken(): void {
    localStorage.removeItem("token");
  }

  getToken(): string {
    return localStorage.getItem("token");
  }

  logout(){
    this.cookieService.delete('token');    
    this.router.navigate(['userlogin']);
  }

  isLoggedIn(){
    
    // var token = this.getToken();
    // console.log('In isLoggedin '+token);
    // console.log('In isLoggedin '+token.length);
    // if(token !== null && token.length > 0){
    //   var payload = JSON.parse(atob(token.split('.')[1]));
    //   console.log("in is Logged in "+payload.role);
    //   console.log(" Expiry "+payload.exp  / 1000+" "+ Date.now() / 1000)
    //   console.log(payload.exp > Date.now() / 1000)
    //   return payload.exp > Date.now() / 1000;
    // }else{
    //   return false;
    // }

  }

  setCurrentUser(user:User):void{
    this.currentUser = user;
  }

  getCurrentUser():User{
    return this.currentUser;
  }

  currentUserDetails(authToken:String): void{
    //if(this.isLoggedIn()){
      
      // var token = this.getToken();
      // var payload = JSON.parse(atob(token.split('.')[1]));
      //   console.log("Payload "+payload._id);

        // this.loggedInUser.email = payload.email,        
        // this.loggedInUser.fullname = payload.fullname,        
        // this.loggedInUser.role = payload.role,        
        // this.loggedInUser.cmpid = payload.cmpid,       
        // this.loggedInUser.uid = payload.uid

      //return this.loggedInUser;
    }
  }  

