import { FormsModule } from '@angular/forms';
import { HttpClient, HttpClientModule } from '@angular/common/http';
import { TodoService } from './services/todo.service';
import {UserService} from './services/user.service';
import {CompanyService} from './services/company.service';
import {DomainService} from './services/domain.service';
import {ApplicationService} from './services/application.service';
import {DomainApplicationUserService} from './services/dmappuser.service';
import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { AppComponent } from './app.component';
import {UserLoginComponent} from './components/userlogin_component/userlogin.component';
import {SuperUserComponent} from './components/superuser_component/superuser.component';
import {DashboardComponent} from './dashboard.component';
import {ProductOwnerComponent} from './productowner.component';
import {ApplicationUserComponent} from './applicationuser.component';
import {Administrator} from './components/administrator/admin.component';
import {CompanyComponent} from './components/company_component/company.component';
import {AppUser} from './components/appuser_component/appuser.component';
import {ProductOwner} from './components/productowner_component/prodowner.component';
import {DomainComponent} from './components/domain_component/domain.component';
import {ApplicationComponent} from './components/application_component/application.component';
import {NgbModule} from '@ng-bootstrap/ng-bootstrap';
import { AlertsService } from 'angular-alert-module';
import { AuthenticationService } from './services/authentication.service';
import { ProjectService } from './services/project.service';
import { CmmaService } from './services/cmma.service';
import {CookieService} from 'ngx-cookie-service';
import { HTTP_INTERCEPTORS } from '@angular/common/http';
import {AuthenticationInterceptor} from './http-interceptors/authentication-interceptor';
import { Provider } from '@angular/compiler/src/core';


export const httpInterceptorProviders:Provider = [
  {provide : HTTP_INTERCEPTORS, useClass: AuthenticationInterceptor, multi:true}
];

const appRoutes: Routes = [
  { path: '', redirectTo: 'userlogin', pathMatch: 'full' },
  { path: 'home',     component: DashboardComponent },
  { path: 'userlogin',component: UserLoginComponent },
  { path: 'superuser',    component: SuperUserComponent },
  { path: 'createpo', component: ProductOwnerComponent },
  { path: 'createau', component: ApplicationUserComponent },
  { path: 'cmmaadmin', component: Administrator },
  { path: 'createcp', component: CompanyComponent },
  { path: 'productowner', component: ProductOwner },
  { path: 'domain', component: DomainComponent },
  { path: 'app', component:ApplicationComponent},
  { path: 'Developer', component:AppUser}
];

@NgModule({
  declarations: [
    AppComponent,
    DashboardComponent,
    UserLoginComponent,
    ProductOwnerComponent,
    ApplicationUserComponent,
    SuperUserComponent,
    Administrator,
    CompanyComponent,
    ProductOwner,
    DomainComponent,
    ApplicationComponent,
    AppUser
  ],
  imports: [
    BrowserModule,
    HttpClientModule,
    FormsModule,
    NgbModule.forRoot(),
    RouterModule.forRoot(appRoutes)
  ],
  providers: [
    TodoService,
    UserService,
    CompanyService,
    CompanyComponent,
    DomainService,
    ApplicationService,
    DomainApplicationUserService,
    AuthenticationService,
    AlertsService,
    ProjectService,
    CmmaService,
    CookieService,
    httpInterceptorProviders    
  ],
  bootstrap: [AppComponent]
})
export class AppModule {}
